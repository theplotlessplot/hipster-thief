﻿using UnityEngine;
using System.Collections;

public class SpriteManager : MonoBehaviour {

	public static SpriteManager use;

	public Sprite[] _spriteChooserCar = new Sprite[4];
	public Sprite[] _spriteChooserTruck= new Sprite[4];
	public Sprite[] _spriteChooserMiniCar = new Sprite[4];

	void Awake()
	{
		
		if(GameObject.Find("SpriteManager") != null)
			if (use) return;
		
		use = this;
		
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
