﻿using UnityEngine;
using System.Collections;

public class AudioManager : MonoBehaviour {

	public static AudioManager use;

	public AudioClip _musicMenu;
	public AudioClip _buttonPressed;
	public AudioClip _musicGame;
	public AudioClip _motoSound;
	public AudioClip _policeSound;
	public AudioClip _alarmSound;
	public AudioClip _crash;

	public AudioSource _soundEffects;
	public AudioSource _music;
	public AudioSource _painInTheAss;

	public bool _On;
	
	void Awake()
	{
		
		if(GameObject.Find("AudioManager") != null)
			if (use) return;
		
		use = this;

		DontDestroyOnLoad (this.gameObject);
	}

	public void ToggleSound(bool status)
	{
		if (status) 
		{
			_On = true;
			_music.mute = false;
			_painInTheAss.mute =false;
			_soundEffects.mute = false;
			_music.volume = 0.06f; //previously 0.1f
			_soundEffects.volume = 0.06f;
			_painInTheAss.volume = 0.06f;



		} else 
		{
			_On = false;
			_music.mute = true;
			_painInTheAss.mute =true;
			_soundEffects.mute = true;
			_music.volume = 0f;
			_soundEffects.volume = 0f;
			_painInTheAss.volume = 0f;
		}

		if (Application.loadedLevelName.CompareTo ("Game") == 0) 
		{
			Thief.use.setAudio (!status);
			Siren.use.setAudio(!status);
		}
	}

	public void MusicToggle(bool status)
	{
		if (status) 
		{
			_On = true;
			_music.mute = false;
			_painInTheAss.mute =false;
			_soundEffects.mute = false;
			_music.volume = 0.06f;
			_soundEffects.volume = 0.06f;
			_painInTheAss.volume = 0.06f;//previously 0.1f
			
		} else 
		{
			_On = false;
			_music.mute = true;
			_painInTheAss.mute =true;
			_soundEffects.mute = true;
			_music.volume = 0f;
			_soundEffects.volume = 0f;
			_painInTheAss.volume = 0f;
		}
	}

	public void PlayButtonSound()
	{
		_soundEffects.clip = _buttonPressed;
		_soundEffects.loop = false;
		_soundEffects.Play ();
	}

	public void PlayCrashSound()
	{
		//TOO FUCKING LOUD!!! Reduce volume when playing crash sound:
		_soundEffects.volume = 0.06f;

		_soundEffects.clip = _crash;
		_soundEffects.loop = false;
		_soundEffects.Play ();
	}

	public void PlayPoliceSound()
	{
		_soundEffects.clip = _policeSound;
		_soundEffects.loop = false;
		_soundEffects.Play ();
	}

	public void PlayMotoSound()
	{
		_painInTheAss.clip = _motoSound;
		_painInTheAss.loop = true;
		_painInTheAss.Play ();
	}

	public void PlayAlarmSound()
	{
		_soundEffects.clip = _alarmSound;
		_soundEffects.loop = false;
		_soundEffects.Play ();
	}

	public void PlayMusicMenu()
	{
		_music.clip = _musicMenu;
		_music.loop = true;
		_music.Play ();
	}

	public void StopMusicMenu()
	{
		_music.clip = _musicMenu;
		_music.Stop ();

	}

	public void StopMotoSound()
	{
		_soundEffects.Stop ();
		_soundEffects.clip = null;


		
	}

	public void StopAll()
	{
		_soundEffects.Stop ();
		_music.Stop ();


	}

	public void PlayMusicIngame()
	{
		
		_music.clip = _musicGame;
		_music.Play ();
	}


	// Use this for initialization
	void Start () {
	

	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
