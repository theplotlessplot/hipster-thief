﻿using UnityEngine;
using System.Collections;

public enum POWER_TYPE
{
	SLOW,
	BOOST,
	NONE

}

public class PowerUp : MonoBehaviour {


	public POWER_TYPE _pwrType;

	private bool _usable;

	public bool usable {
		get {
			return _usable;
		}
		set {
			_usable = value;
		}
	}

	// Use this for initialization
	void Start () {
	
		Hud.DestroyPowerUp += DestroyPowerUp;

	}

	void DestroyPowerUp()
	{
		GameObject.Destroy (this.gameObject);
		}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OnTriggerEnter(Collider other)
	{
		//Debug.Log ("OTHER : " + other.name);
		if (other.name.CompareTo ("Thief") == 0 && _pwrType == POWER_TYPE.SLOW && !Thief.use.isPower) 
		{
			Thief.use.isPower = true;
			Hud.use.UseSlow();
			this.gameObject.SetActive(false);
			usable = true;
		}

		if (other.name.CompareTo ("Thief") == 0 && _pwrType == POWER_TYPE.BOOST && !Thief.use.isPower) 
		{
			Thief.use.isPower = true;
			Hud.use.UseSpeed();
			this.gameObject.SetActive(false);
			usable = true;
		}
		#if UNITY_ANDROID
		Social.ReportProgress("CgkIqoCS9doCEAIQFg", 100, null);
		#endif

	}
}
