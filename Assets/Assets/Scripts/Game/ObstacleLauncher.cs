﻿using UnityEngine;
using System.Collections;

public class ObstacleLauncher : MonoBehaviour { 

	GameObject _trashCan;
	//private int lastColor = -1;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	string IntegerToObstacle(int i)
	{
		if (Thief.use.isPower) 
		{
			i = Mathf.Clamp(i, 0, 4);
		}


		switch (i) 
		{
		case 0:
			return "Car";
		case 1:
			return "Bus";
		case 2:
			return "Truck";
		case 3:
			return "Police";
		case 4:
			return "MiniCar";
		case 5:
			return "Signs";
		case 6:
			return "Ghost";
		case 7:
			return "Slow";
			
		default : return "";
		}
	}
	
	public void LaunchObstacle()
	{
				if (GameObject.Find ("TrashCan") == null)
						GameLogic.use._trashCan = new GameObject ("TrashCan");

				GameLogic gl = Hud.use.GetComponent<GameLogic> ();

				//Debug.Log ("gl.actualLevel: " + gl.actualLevel);
				int[] g = gl.levels [gl.actualLevel].probs;

	
				int i = Random.Range (0, gl.levels [gl.actualLevel].probs.Length - 1);

				GameObject go = Instantiate (Resources.Load ("Prefabs/" + IntegerToObstacle (
			g [i])))
 as GameObject;


			SwitchTexture (go);
				go.transform.parent = GameLogic.use._trashCan.transform;
				//go.name = IntegerToObstacle(i);
//				go.GetComponent<Rigidbody> ().drag = gl.levels [gl.actualLevel]._drag;
//				go.GetComponent<Obstacle> ()._drag = gl.levels [gl.actualLevel]._drag;
				go.transform.localPosition = this.transform.position;

		}

	public void SwitchTexture(GameObject go)
	{
		int actualColor = Random.Range (0, 3);

		while (Hud.use._lastColor == actualColor) 
		{
				actualColor = Random.Range (0, 3);
		}

		Hud.use._lastColor = actualColor;

		if (go.GetComponent<Obstacle> ()._type == OBSTACLE_TYPE.CAR) 
		{
			go.GetComponent<SpriteRenderer>().sprite = SpriteManager.use._spriteChooserCar[actualColor];
		}

		if (go.GetComponent<Obstacle> ()._type == OBSTACLE_TYPE.TRUCK) 
		{
			go.GetComponent<SpriteRenderer>().sprite = SpriteManager.use._spriteChooserTruck[actualColor];
		}

		if (go.GetComponent<Obstacle> ()._type == OBSTACLE_TYPE.MINICAR) 
		{
			go.GetComponent<SpriteRenderer>().sprite = SpriteManager.use._spriteChooserMiniCar[actualColor];
		}
	}

}
