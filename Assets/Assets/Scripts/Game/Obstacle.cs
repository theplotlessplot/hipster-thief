﻿using UnityEngine;
using System.Collections;

public enum OBSTACLE_TYPE
{
	CAR = 0,
	BUS = 1,
	TRUCK = 2,
	POLICE = 3,
	SIGNS = 5,
	MINICAR = 4,
	SLOW = 6,
	BOOST = 7

}

public class Obstacle : MonoBehaviour {

	public OBSTACLE_TYPE _type;
	public int _points;

	public float _mass;
	public float _drag;

	private int _activeSign;
	private Vector3 initialPosition;
	private bool _usable = true;

	public TrailRenderer _trail;

	private bool _trailOn = false;

	public OBSTACLE_TYPE type 
	{
		get { return _type; }
		set { _type = value; }
	}

	public int points 
	{
		get { return _points; } 
		set { _points = value; }
	}

	public bool usable {
		get {
			return _usable;
		}
		set {
			_usable = value;
		}
	}

	void OnEnable()
	{
		if (_type != OBSTACLE_TYPE.BOOST && _type != OBSTACLE_TYPE.SLOW) 
		{
			Hud.OnToggle += OnTrail;
			Hud.OffToggle += OffTrail;
			if(_trail != null)
			_trail.enabled = Hud.use._isSlow;

		}
	}

	void OnDisable()
	{
		/*if (_type != OBSTACLE_TYPE.BOOST && _type != OBSTACLE_TYPE.SLOW) 
		{
			Hud.OnToggle -= OnTrail;
			Hud.OffToggle -= OffTrail;
			
		}*/
	}

	void OnTrail()
	{
		if(_trail != null)
		_trail.enabled = true;
	}

	void OffTrail()
	{
		if(_trail != null)
		_trail.enabled = false;
	}

	public void OnTriggerExit(Collider other)
	{
		if (other.transform.tag.CompareTo ("OutOfBounds") == 0) 
		{
			usable = true;

			GameObject.Destroy(this.gameObject);
		}
	}

	public void OnTriggerEnter(Collider other)
	{
		//Debug.Log ("OTHER : " + other.name);
		if (other.name.CompareTo ("Thief") == 0 &&_type != OBSTACLE_TYPE.SLOW && _type != OBSTACLE_TYPE.BOOST && !Thief.use._blinking)
		{

			if(_type == OBSTACLE_TYPE.POLICE)
			{
#if UNITY_IPHONE
				GameCenterSingleton.Instance.ReportAchievementProgress ("HT_Achievement_20", 100);
#elif UNITY_ANDROID
				Social.ReportProgress("CgkIqoCS9doCEAIQCg", 100, null);
#endif
			}

			if(_type == OBSTACLE_TYPE.BUS)
			{
				#if UNITY_IPHONE
				GameCenterSingleton.Instance.ReportAchievementProgress ("HT_Achievement_19", 100);
				#elif UNITY_ANDROID
				Social.ReportProgress("CgkIqoCS9doCEAIQCQ", 100, null);
				#endif
			}
			Thief.use._blinking = true;
			this.gameObject.SetActive(false);
			usable = true;
			Hud.use.DecrementLines();
			Thief.use._spriteRenderer.gameObject.transform.localPosition.Set(0f, Thief.use._spriteRenderer.transform.localPosition.y, Thief.use._spriteRenderer.transform.localPosition.z);
			AudioManager.use.PlayCrashSound();
			Thief.use.ActiveSign();
			iTweenEvent.GetEvent(Thief.use._spriteRenderer.gameObject, "Fade").Play();
		}
	}

	// Use this for initialization
	void Start () 
	{	

	}
	
	// Update is called once per frame
	void Update () 
	{ 

	}

	void FixedUpdate () {
		_drag = GameLogic.use._gdrag;
		this.GetComponent<Rigidbody>().drag = _drag;
	}

}