﻿using UnityEngine;
using System.Collections;

public class Thief : MonoBehaviour 
{
	public static Thief use;
	public GameObject _playa;
		
	private bool _character;
	
	private Vector3 wantedPos;
	private Vector3 uncheckedPos;
	private Vector3 finalPos;
	private bool _firstTime = true;
	private bool _isPausePressed;
	private int i = 0;
	private Vector3 _oldScale;
	public SpriteRenderer _spriteRenderer;
	public GameObject _lights;
	public GameObject _leftWing;
	public GameObject _rightWing;
	public GameObject _rocket;
	public bool _blinking = false;
	public int _activeSign;

	public AudioSource _audio;

	public bool isPower = false;

	public GameObject[] _crashSigns = new GameObject[4];

//	public AudioSource _explosionSound;
//	public AudioSource _motoSound;
//	public AudioSource _powerUpSound;

	float screenDiagonalSize;
	float minSwipeDistancePixels;
	bool touchStarted;
	Vector2 touchStartPos;
	public float minSwipeDistance;


	public GameObject[] _lanes = new GameObject[3];
	private int _lane = 1;

	public GameObject _sprite;
	public Vector3 _newScale = new Vector3(0.015f,0.015f,0.015f);

	public bool isSwipe;

	private Camera GUICam;
	public bool character 
	{
		get { return _character; }
		set { _character = value; }
	}
	
	public bool firstTime 
	{
		get { return _firstTime; }
		set { _firstTime = value; }
	}
	
	// Use this for initialization
	
	void Awake()
	{
		
		if(GameObject.Find("Thief") != null)
			if (use) return;
		
		use = this;

		_oldScale = this.gameObject.transform.localScale;

		if (AudioManager.use != null) 
		{
		//	Debug.Log ("_ON: " + AudioManager.use._On);

			_audio.mute = !AudioManager.use._On;
//		_motoSound.Play ();
		}
	}

	public void setAudio(bool flag) 
	{
		_audio.mute = !flag;
	}

	public void DisableSigns()
	{
		_crashSigns [0].SetActive (false);
		_crashSigns [1].SetActive (false);
		_crashSigns [2].SetActive (false);
		_crashSigns [3].SetActive (false);
	}

	public void Fade()
	{
		iTweenEvent.GetEvent (this.gameObject, "Fade").Play ();
	}

	public void SwapRight()
	{
		if (_lane >= 2)
			return;

		else 
		{
			_lane++;
			//iTween.MoveTo(this.gameObject, _lanes[_lane].transform.localPosition, 0.25f);
			iTween.MoveTo(this.gameObject, _lanes[_lane].transform.localPosition, 0.25f);
			iTween.MoveTo (_lights, iTween.Hash ("x", _lanes[_lane].transform.localPosition.x, "time", 0.8f, "delay", 0.2f));
			//Thief.use.transform.localPosition = _lanes[_lane].transform.localPosition;
		}

	}

	public void SwapLeft()
	{
		if (_lane <= 0)
			return;
		
		else 
		{
			_lane--;
			iTween.MoveTo(this.gameObject, _lanes[_lane].transform.localPosition, 0.25f);
			iTween.MoveTo (_lights, iTween.Hash ("x", _lanes[_lane].transform.localPosition.x, "time", 0.8f, "delay", 0.2f));
			//Thief.use.transform.localPosition = _lanes[_lane].transform.localPosition;
		}
		
	}



	// Update is called once per frame
	void Update () 
	{ 
		#if UNITY_EDITOR
		if(Input.GetMouseButtonDown(0) && !GameLogic.use.pause && isSwipe && !touchStarted){
			if(GameLogic.use._firstTime)
				GameLogic.use.GameLaunch();
			touchStarted = true;
			touchStartPos = Input.mousePosition;
		}

		if(Input.GetMouseButtonUp(0) && !GameLogic.use.pause && isSwipe && touchStarted){
			Vector2 lastPos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

			touchStarted = false;
			TestForSwipeGesture(lastPos);

		}
		#endif

		#if UNITY_ANDROID || UNITY_IOS 
		if (Input.touchCount >  0 && !GameLogic.use.pause && isSwipe) {
			if (Input.touchCount > 0) {
				Touch touch = Input.touches[0];
				
				switch (touch.phase) {
					
					case TouchPhase.Began:
						touchStarted = true;
						touchStartPos = touch.position;
						break;
						
					case TouchPhase.Ended:
						break;

					case TouchPhase.Moved:
						if (touchStarted) {
							if(GameLogic.use._firstTime)
								GameLogic.use.GameLaunch();

							TestForSwipeGesture(touch.position);
							touchStarted = false;
						}
						break;
						
					case TouchPhase.Canceled:
						touchStarted = false;
						break;
				}
			}
		}
		#endif

	}

	void TestForSwipeGesture(Vector2 touch){
		Vector2 lastPos = touch;

		if (lastPos.x > touchStartPos.x && Mathf.Abs(lastPos.x - touchStartPos.x) > minSwipeDistance && lastPos.y < 0.9* 
		    Screen.height) {
			SwapLeft();
		} else if (lastPos.x < touchStartPos.x && Mathf.Abs(lastPos.x - touchStartPos.x) > minSwipeDistance && lastPos.y < 0.9* 
		           Screen.height) {
			SwapRight();
		}

	}

	public void BoostOn()
	{
		//_spriteRenderer.transform.localScale = _newScale;
		iTween.ScaleTo (this.gameObject, new Vector3(0.17f,0.17f,0.17f), 0.7f);
		_leftWing.GetComponent<Renderer>().enabled = true;
		_rightWing.GetComponent<Renderer>().enabled = true;
		_rocket.GetComponent<Renderer>().enabled = true;
		iTweenEvent.GetEvent (_rightWing, "OpenWings").Play();
		iTweenEvent.GetEvent (_leftWing, "OpenWings").Play();

		_spriteRenderer.sortingOrder = -1;
		isPower = true;

	}

	public void BoostOff()
	{
		_spriteRenderer.sortingOrder = -3;
		iTween.ScaleTo (this.gameObject, _oldScale, 0.7f);
		iTweenEvent.GetEvent (_rightWing, "CloseWings").Play();
		iTweenEvent.GetEvent (_leftWing, "CloseWings").Play();
		//_spriteRenderer.transform.localScale = _oldScale;
		isPower = false;
	}

	public void DestroySign()
	{
		Thief.use._crashSigns [_activeSign].GetComponent<SpriteRenderer> ().enabled = false;
	}

	public void DisableSign()
	{
		iTweenEvent.GetEvent(Thief.use._crashSigns[_activeSign], "Fade").Stop();
		iTweenEvent.GetEvent(Thief.use._crashSigns[_activeSign], "Scale").Stop();
		DestroySign ();

	}

	public void ActiveSign()
	{
		_activeSign = UnityEngine.Random.Range(0, 3);
		Thief.use._crashSigns [_activeSign].GetComponent<SpriteRenderer> ().enabled = true;
	}

	public void RestartAlpha()
	{
		Thief.use._spriteRenderer.GetComponent<Renderer>().material.color = new Color(1f,1f,1f,1f);
		_blinking = false;

	}

	public void SetOff()
	{
		_leftWing.GetComponent<Renderer>().enabled = false;
		_rightWing.GetComponent<Renderer>().enabled = false;
		_rocket.GetComponent<Renderer>().enabled = false;

		Thief.use.GetComponent<BoxCollider> ().GetComponent<Collider>().enabled = true;
		

	}
}