﻿using UnityEngine;
using System.Collections;

public class Siren : MonoBehaviour {

	public static Siren use;

	public GameObject _left, _right, _center;

	public AudioSource _audio;

	void Awake()
	{
		if(GameObject.Find("Sirenes") != null)
			if (use) return;
		
		use = this;
	}

	// Use this for initialization
	void Start () {
		if(AudioManager.use != null)
		_audio.mute = !AudioManager.use._On;
	
	}

	public void setAudio(bool flag) 
	{
		_audio.mute = !flag;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
