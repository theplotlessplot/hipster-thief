﻿using UnityEngine;
using System.Collections;

public class Level : MonoBehaviour {

	public int[] _probs = new int[5];

	public float _delayBetweenLaunches;

	public float _drag;

	public int[] probs 
	{
		get {
			return _probs;
		}
		set {
			_probs = value;
		}
	}

	public float delayBetweenLaunches {
		get {
			return _delayBetweenLaunches;
		}
		set {
			_delayBetweenLaunches = value;
		}
	}

	// Use this for initialization
	void Start () 
	{

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	string IntegerToObstacle(int i)
	{
		switch (i) 
		{
			case 0:
				return "Car";
			case 1:
				return "Bus";
			case 2:
				return "Truck";
			case 3:
				return "Police";
			case 4:
				return "Hole";
			case 5:
				return "Signs";
			case 6:
				return "Ghost";
			case 7:
				return "Slow";

			default : return "";
		}
	}
	
	public void PopulateArray()
	{

		for (int i = 0, k = 0; i < _probs.Length; i++) 
		{

			for(int j = 0 + k, l = 0; l < probs[i]; j++, l++)
			{

				Debug.Log ("OBS : " + IntegerToObstacle(i));
				GameObject go = Instantiate(Resources.Load ("Prefabs/" + IntegerToObstacle(i))) as GameObject;
				go.name = IntegerToObstacle(i);
				go.GetComponent<Rigidbody>().drag = _drag;
				go.GetComponent<Obstacle>()._drag = _drag;
				go.SetActive(false);

				k = j + 1;
			}

		}
	}
}
