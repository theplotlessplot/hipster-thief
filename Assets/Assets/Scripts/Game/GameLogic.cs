﻿using UnityEngine;
using System.Collections;

public class GameLogic : MonoBehaviour 
{
	public static GameLogic use;
	public TextMesh _restart;

	public GameObject _trashCan;

	public Level[] _levels;

	public bool _firstTime = true;

	private int _actualLevel = 0;

	private bool _lastChance = true;
	private bool _pause = false;

	public ObstacleLauncher[] _obstacleLauncher;
	public int interval;
	public float minDelayBetweenLaunches;
	public float minDrag;
	public float dragMultiplier;
	public float delayMultiplier;

	public float _gdrag;
	public float _gdelayBetweenLaunches;

	private int _countDown = 3;

	public int actualLevel {
		get {
			return _actualLevel;
		}
		set {
			_actualLevel = value;
		}
	}

	public Level[] levels {
		get {
			return _levels;
		}
		set {
			_levels = value;
		}
	}

	public bool pause {
		get {
			return _pause;
		}
		set {
			_pause = value;
		}
	}

	public bool lastChance {
		get {
			return _lastChance;
		}
		set {
			_lastChance = value;
		}
	}
	
	void Awake()
	{
		//Set target framerate to 60fps
		Application.targetFrameRate = 60;

		if(GameObject.Find("Hud") != null)
			if (use) return;
		
		use = this;
		if(AudioManager.use != null)
		AudioManager.use.StopMusicMenu ();
	}

	// Use this for initialization
	void Start () { }
	
	// Update is called once per frame
	void Update () 
	{ 
			if (int.Parse(Hud.use._score.text) > 0 && int.Parse(Hud.use._score.text) % interval == 0) {
				
				if (_gdrag >= minDrag) {
					_gdrag -= _gdrag * dragMultiplier;
					_levels[_actualLevel]._drag = _gdrag;
				}
				
				if (_gdelayBetweenLaunches >= minDelayBetweenLaunches) {
					_gdelayBetweenLaunches -= _gdelayBetweenLaunches * delayMultiplier;
					_levels[_actualLevel]._delayBetweenLaunches = _gdelayBetweenLaunches;
				}
			}
	}

	IEnumerator LaunchSequence()
	{
		while(true)
		{
			ObstacleLauncher bl = _obstacleLauncher[Random.Range(0, _obstacleLauncher.Length)];


			yield return new WaitForSeconds(_levels[_actualLevel].delayBetweenLaunches); //yield return new WaitForSeconds(1 + _levels[0].delayBetweenLaunches);
			bl.LaunchObstacle();
		}
	}

	void CountDown()
	{
		_countDown--;
		_restart.text = _countDown.ToString();

		Debug.Log ("Countdown: " + _countDown);

		if (_countDown == 0) 
		{
			CancelInvoke();
			_countDown = 3;
			_restart.gameObject.SetActive(false);
			_restart.text = _countDown.ToString();

			StartCoroutine("LaunchSequence");


		}

	}

	public void StartLaunch()
	{
		StartCoroutine("LaunchSequence");
	}

	public void StopLaunch()
	{
		StopAllCoroutines ();
		StopCoroutine("LaunchSequence");
	}

	public void GameLaunch()
	{
		_firstTime = false;
		Hud.use._tutorial.gameObject.SetActive (false);
		StartCoroutine (LaunchSequence ());
	}

	public void ToggleRigidBodies(bool status)
	{
		foreach (Transform obstacle in _trashCan.transform) 
		{
			Debug.Log ("TOGGLE RIGIDBODY:" + obstacle.name);
				GameObject.Destroy(obstacle.gameObject);
		}
	}

	public void DestroyObstacles()
	{

		}

	public void ExtraChance()
	{
				Time.timeScale = 1f;

				Hud.use.StopPowerUps ();




				if (Hud.use._popUp != null)
						GameObject.Destroy (Hud.use._popUp);
				_restart.gameObject.SetActive (true);
				_restart.text = _countDown.ToString ();



				Debug.Log ("Extra Chance");
	
				InvokeRepeating ("CountDown", 1, 1f);

				AudioManager.use._music.mute = !AudioManager.use._On;
				AudioManager.use._painInTheAss.mute = !AudioManager.use._On;
				AudioManager.use._soundEffects.mute = !AudioManager.use._On;
				Thief.use._audio.mute = !AudioManager.use._On;
				Siren.use._audio.mute = !AudioManager.use._On;

				if (Hud.use._endMenu != null)
						Hud.use.DestroyEndMenu ();

				//Hud.use._score.text = "0";
				Hud.use.nlifes = 3;
				//GameLogic.use.pause = false;
				Hud.use._lifes.text = Hud.use.nlifes.ToString ();
				Thief.use._playa.transform.localPosition = new Vector3 (0f, 0f, 0f);

		}
}
