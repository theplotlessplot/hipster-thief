﻿using UnityEngine;
using System.Collections;

public class StoreMenuHandler : MonoBehaviour 
{
	// Use this for initialization
	void Start () { }

	public void TapItemOne()
	{
		AudioManager.use.PlayButtonSound ();
		SendMessage ("BuyItemOne", SendMessageOptions.RequireReceiver);
	}

	public void TapItemTwo()
	{
		AudioManager.use.PlayButtonSound ();
		SendMessage ("BuyItemTwo", SendMessageOptions.RequireReceiver);
	}

	public void TapItemThree()
	{
		AudioManager.use.PlayButtonSound ();
		SendMessage ("BuyItemThree", SendMessageOptions.RequireReceiver);
	}

	public void TapBackButton()
	{
		AudioManager.use.PlayButtonSound ();
		if(Application.loadedLevelName.CompareTo("Menu") == 0)
			SendMessage ("BackToMenu", SendMessageOptions.DontRequireReceiver);

		else SendMessage ("BackToGameOver", SendMessageOptions.DontRequireReceiver);

	}
	// Update is called once per frame
	void Update () { }
}