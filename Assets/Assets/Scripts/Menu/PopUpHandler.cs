﻿using UnityEngine;
using System.Collections;

public class PopUpHandler : MonoBehaviour {

	// Use this for initialization
	void Start () { }
	
	// Update is called once per frame
	void Update () {}

	void TapYesButton()
	{
		SendMessage ("YesButton", SendMessageOptions.DontRequireReceiver);
	}

	void TapNoButton()
	{
		SendMessage ("NoButton", SendMessageOptions.DontRequireReceiver);
	}
}
