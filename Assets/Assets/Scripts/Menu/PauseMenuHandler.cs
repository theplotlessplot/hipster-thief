﻿using UnityEngine;
using System.Collections;

public class PauseMenuHandler : MonoBehaviour 
{
	public void TapRestartButton()
	{
		AudioManager.use.PlayButtonSound ();
		SendMessage("RestartGame", SendMessageOptions.DontRequireReceiver);
	}
	
	public void TapQuitButton()
	{
		AudioManager.use.PlayButtonSound ();
		SendMessage("GoToMenu", SendMessageOptions.DontRequireReceiver);
	}
	
	public void TapResumeButton()
	{
		AudioManager.use.PlayButtonSound ();
		SendMessage("ResumeGame", SendMessageOptions.DontRequireReceiver);
	}

	public void TapSoundButton()
	{
		AudioManager.use.PlayButtonSound ();
		SendMessage ("ToggleSfx", SendMessageOptions.DontRequireReceiver);
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
