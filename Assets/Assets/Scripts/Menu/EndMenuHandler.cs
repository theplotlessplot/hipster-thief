﻿using UnityEngine;
using System.Collections;

public class EndMenuHandler : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void TapShareButton()
	{
		AudioManager.use.PlayButtonSound ();
		SendMessage ("Share", SendMessageOptions.DontRequireReceiver);
	}

	public void TapPlayButton()
	{
		AudioManager.use.PlayButtonSound ();
		SendMessage("RestartGame", SendMessageOptions.DontRequireReceiver);
	}
	
	public void TapQuitButton()
	{
		AudioManager.use.PlayButtonSound ();
		SendMessage("GoToMenu", SendMessageOptions.DontRequireReceiver);
	}
}
