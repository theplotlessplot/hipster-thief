﻿using UnityEngine;
using System.Collections;
using UnityEngine.Advertisements;

public enum POPUP_STATES
{
	AD_COLONY,
	WINGZ
}

public class PopUp : MonoBehaviour {
	

	public POPUP_STATES _state = POPUP_STATES.AD_COLONY;

	public GameObject _adColonyBg;
	public GameObject _wingsBg;

	void Start () 
	{
		ToggleBackGround ();
	}

	public void ToggleBackGround()
	{
		if(_state == POPUP_STATES.AD_COLONY)
		{
			_adColonyBg.SetActive(true);
			_wingsBg.SetActive(false);

		}
		else
		{
			_wingsBg.SetActive(true);
			_adColonyBg.SetActive(false);
		}
	}
	
	// Update is called once per frame
	void Update () { }

	public void YesButton()
	{
		if (_state == POPUP_STATES.AD_COLONY && Advertisement.IsReady("rewardedVideo")) 
		{
			Debug.Log ("Tocar Add");
			var options = new ShowOptions { resultCallback = HandleShowResult };
			Advertisement.Show("rewardedVideo", options);

		} else 
	{
			PlayerPrefs.SetInt("LIFES", PlayerPrefs.GetInt("LIFES",0) - 1);
			Hud.use._wings.text = PlayerPrefs.GetInt("LIFES",0).ToString();


			GameLogic.use.ExtraChance();
			GameObject.Destroy(this.gameObject);
		}
	}

	private void HandleShowResult(ShowResult result)
	{
		switch (result)
		{
		case ShowResult.Finished:
			Debug.Log("The ad was successfully shown.");
			GameLogic.use.ExtraChance ();
			//
			// YOUR CODE TO REWARD THE GAMER
			// Give coins etc.
			break;
		case ShowResult.Skipped:
			Debug.Log("The ad was skipped before reaching the end.");
			break;
		case ShowResult.Failed:
			Debug.LogError ("The ad failed to be shown.");

			break;
		}
	}
	public void NoButton()
	{
		if (Hud.use._endMenu == null) {
						GameObject _endScreen = Instantiate (Resources.Load ("Prefabs/EndMenu")) as GameObject;
			Hud.use._endMenu = _endScreen;
				}
		GameObject.Destroy (this.gameObject);
	}

	public void GoToStore()
	{
		if (Hud.use._endMenu == null) {
						GameObject go = Instantiate (Resources.Load ("Prefabs/StoreMenu")) as GameObject;
				}
		GameObject.Destroy(this.gameObject);
	}

}
