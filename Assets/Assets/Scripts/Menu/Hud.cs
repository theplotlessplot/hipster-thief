﻿using UnityEngine;
using System.Collections;
using UnityEngine.Advertisements;

public class Hud : MonoBehaviour {
	
	public static Hud use;
	
	private GameObject _pauseMenu;
	public GameObject _endMenu;
	public GameObject _powerUpBar;
	public TextMesh _lifes, _wings;
	public TextMesh _score;
	public GameObject _tutorial;
	public AudioSource _music;
	public AudioSource _honks;
	public AudioSource _police;
	public GameObject _bar;
	private int _nlifes;
	private Vector3 _initialScale;
	public int _lastColor = -1;
	public bool _powerUp = false;
	private int temp = 0;
	public bool _isSlow;
	public GameObject _store;

	public GameObject _popUp;

	public int nlifes {
		get {
			return _nlifes;
		}
		set {
			_nlifes = value;
		}
	}

	private int _iscore;

	public int iscore {
		get {
			return _iscore;
		}
		set {
			_iscore = value;

		}
	}

	public delegate void ToggleOn();
	public static event ToggleOn OnToggle;
	public delegate void ToggleOff();
	public static event ToggleOff OffToggle;
	public delegate void PowerUp();
	public static event PowerUp DestroyPowerUp;

	void Awake()
	{
		if(GameObject.Find("Hud") != null)
			if (use) return;
		
		use = this;

		_initialScale = _bar.transform.localScale;
	}

	void Restart()
	{
		StartCoroutine (HereComesAgain ());
		iTweenEvent.GetEvent (this.gameObject, "Honks").Play();
	}

	IEnumerator HereComesAgain() { yield return new WaitForSeconds (Random.Range (2, 5)); }

	// Use this for initialization
	void Start () 
	{
		_nlifes = 3; //original = 3
		_iscore = 0;
		temp = PlayerPrefs.GetInt("OVERALL_SCORE",0 );
		_wings.text = PlayerPrefs.GetInt ("LIFES", 0).ToString();
		iTweenEvent.GetEvent (_tutorial, "Blink").Play ();

		if (AudioManager.use != null) 
		{
			AudioManager.use.StopMotoSound ();
			AudioManager.use.PlayMusicIngame ();
		}
		
		AudioManager.use._music.pitch = Time.timeScale;
		AudioManager.use._painInTheAss.pitch =Time.timeScale;;
		AudioManager.use._soundEffects.pitch = Time.timeScale;
		AudioManager.use._music.mute = !AudioManager.use._On;
		AudioManager.use._soundEffects.mute = !AudioManager.use._On;
		//AudioManager.use.PlayPoliceSound ();
	}

	IEnumerator Delay()
	{
		yield return new WaitForSeconds (1f);

		_music.Play ();
//		_police.Play ();

	}
	
	// Update is called once per frame
	void Update () 
	{
		if (!GameLogic.use._firstTime && nlifes > 0 && !GameLogic.use.pause ) 
		{
			_iscore++;
			temp++;
			PlayerPrefs.SetInt ("OVERALL_SCORE", temp);
		//	PlayerPrefs.Save ();
			SwapTextSize();
			_score.text = iscore.ToString ();
			CheckAchievements(_iscore);
			ChangeLevel(_iscore);
		}
	}

	void CheckAchievements(int score)
	{
		if (score == 1000) 
		{
			#if UNITY_IPHONE
			GameCenterSingleton.Instance.ReportAchievementProgress ("BabySteps", 100);
			#elif UNITY_ANDROID
			if(Social.localUser.authenticated)
			Social.ReportProgress("CgkIqoCS9doCEAIQAQ", 100, null);
			#endif
		}

		else if (score == 5000) 
		{
			#if UNITY_IPHONE
			GameCenterSingleton.Instance.ReportAchievementProgress ("HT_Achievement_2", 100);
			#elif UNITY_ANDROID
			if(Social.localUser.authenticated)
			Social.ReportProgress("CgkIqoCS9doCEAIQAg", 100, null);
			#endif
		}

		else if (score == 10000) 
		{
			#if UNITY_IPHONE
			GameCenterSingleton.Instance.ReportAchievementProgress ("HT_Achievement_3", 100);
			#elif UNITY_ANDROID
			if(Social.localUser.authenticated)
				Social.ReportProgress("CgkIqoCS9doCEAIQAw", 100, null);
			#endif
		}

		else if (score == 15000) 
		{
			#if UNITY_IPHONE
			GameCenterSingleton.Instance.ReportAchievementProgress ("HT_Achievement_4", 100);
			#elif UNITY_ANDROID
			if(Social.localUser.authenticated)
			Social.ReportProgress("CgkIqoCS9doCEAIQDQ", 100, null);
			#endif
		}

		else if (score == 25000) 
		{
			#if UNITY_IPHONE
			GameCenterSingleton.Instance.ReportAchievementProgress ("HT_Achievement_5", 100);
			#elif UNITY_ANDROID
			if(Social.localUser.authenticated)
			Social.ReportProgress("CgkIqoCS9doCEAIQDg", 100, null);
			#endif
		}

		else if (score == 45000) 
		{
			#if UNITY_IPHONE
			GameCenterSingleton.Instance.ReportAchievementProgress ("HT_Achievement_6", 100);
			#elif UNITY_ANDROID
			if(Social.localUser.authenticated)
			Social.ReportProgress("CgkIqoCS9doCEAIQDw", 100, null);
			#endif
		}
		else if (score == 50000) 
		{
			#if UNITY_IPHONE
			GameCenterSingleton.Instance.ReportAchievementProgress ("HT_Achievement_7", 100);
			#elif UNITY_ANDROID
			if(Social.localUser.authenticated)
			Social.ReportProgress("CgkIqoCS9doCEAIQEA", 100, null);
			#endif

		}

		if (temp == 5000) 
		{
			#if UNITY_IPHONE
			GameCenterSingleton.Instance.ReportAchievementProgress ("HT_Achievement_8", 100);
			#elif UNITY_ANDROID
			if(Social.localUser.authenticated)
			Social.ReportProgress("CgkIqoCS9doCEAIQEQ", 100, null);
			#endif

		}
		else if (temp == 10000) 
		{
			#if UNITY_IPHONE
			GameCenterSingleton.Instance.ReportAchievementProgress ("HT_Achievement_9", 100);
			#elif UNITY_ANDROID
			if(Social.localUser.authenticated)
			Social.ReportProgress("CgkIqoCS9doCEAIQEg", 100, null);
			#endif
		}
		else if (temp == 50000) 
		{
			#if UNITY_IPHONE
			GameCenterSingleton.Instance.ReportAchievementProgress ("HT_Achievement_10", 100);
			#elif UNITY_ANDROID
			if(Social.localUser.authenticated)
			Social.ReportProgress("CgkIqoCS9doCEAIQBQ", 100, null);
			#endif
			}
		else if (temp == 100000) 
		{
			#if UNITY_IPHONE
			GameCenterSingleton.Instance.ReportAchievementProgress ("HT_Achievement_11", 100);
			#elif UNITY_ANDROID
			if(Social.localUser.authenticated)
			Social.ReportProgress("CgkIqoCS9doCEAIQEw", 100, null);
			#endif
		}
		else if (temp == 250000) 
		{
			#if UNITY_IPHONE
			GameCenterSingleton.Instance.ReportAchievementProgress ("HT_Achievement_12", 100);
			#elif UNITY_ANDROID
			if(Social.localUser.authenticated)
			Social.ReportProgress("CgkIqoCS9doCEAIQFA", 100, null);
			#endif
		}
		else if (temp == 500000)
		{
			#if UNITY_IPHONE
			GameCenterSingleton.Instance.ReportAchievementProgress ("HT_Achievement_13", 100);
			#elif UNITY_ANDROID
			if(Social.localUser.authenticated)
			Social.ReportProgress("CgkIqoCS9doCEAIQFQ", 100, null);
			#endif
		}
		else if (temp == 1000000) 
		{
		
			#if UNITY_IPHONE
			GameCenterSingleton.Instance.ReportAchievementProgress ("HT_Achievement_14", 100);
			#elif UNITY_ANDROID
			if(Social.localUser.authenticated)
			Social.ReportProgress("CgkIqoCS9doCEAIQBA", 100, null);
			#endif
		}
	}

	void ChangeLevel(int i)
	{
		if(i >= 0 && i <= 500)
			GameLogic.use.actualLevel = 0;

		else if(i >= 501 && i <= 2000)
			GameLogic.use.actualLevel = 1;

		else if(i >= 2001 && i <= 4000)
			GameLogic.use.actualLevel = 2;

		else if(i >= 4001 && i <= 8000)
			GameLogic.use.actualLevel = 3;

		else if(i >= 8001 && i <= 14000)
			GameLogic.use.actualLevel = 4;

		else if(i >= 14001 && i <= 20000)
			GameLogic.use.actualLevel = 5;

		else if(i >= 20001 && i <= 35000)
			GameLogic.use.actualLevel = 6;

		else GameLogic.use.actualLevel = 7;
	}

	public void GoToPause()
	{

		if (_endMenu != null)
						return;


		if (_pauseMenu == null) {
						Time.timeScale = 0f;
						GameLogic.use.pause = true;
						_pauseMenu = Instantiate (Resources.Load ("Prefabs/PauseMenu")) as GameObject;
				} else {

					Time.timeScale = 1f;
			GameLogic.use.pause = false;
			DestroyPauseMenu();
				}
	}

	public void GoToEnd()
	{
		if (_endMenu == null) 
		{
			PlayerPrefs.Save ();
			_endMenu = Instantiate(Resources.Load("Prefabs/EndMenu")) as GameObject;
		}
	}

	public void DecrementLines()
	{
				

				nlifes--;

				_lifes.text = nlifes.ToString ();

				Thief.use._sprite.GetComponent<iTweenEvent> ().Play ();

				//Debug.Log ("LAST CHANCE: " + GameLogic.use.lastChance + " --- nfiles: " + nlifes + " ---- :" + PlayerPrefs.GetInt ("LIFES", 0));


				bool hasAds = false;


				if (nlifes <= 0) {

			/*using UnityEngine;
			using UnityEngine.Advertisements;

			public class UnityAdsExample : MonoBehaviour
			{
				public void ShowRewardedAd()
				{
					if (Advertisement.IsReady("rewardedVideoZone"))
					{
						var options = new ShowOptions { resultCallback = HandleShowResult };
						Advertisement.Show("rewardedVideoZone", options);
					}
				}

				private void HandleShowResult(ShowResult result)
				{
					switch (result)
					{
					case ShowResult.Finished:
						Debug.Log("The ad was successfully shown.");
						//
						// YOUR CODE TO REWARD THE GAMER
						// Give coins etc.
						break;
					case ShowResult.Skipped:
						Debug.Log("The ad was skipped before reaching the end.");
						break;
					case ShowResult.Failed:
						Debug.LogError("The ad failed to be shown.");
						break;
					}
				}
			}*/
//			Debug.Log ("Akiasasas");
			hasAds = Advertisement.IsReady("rewardedVideo");
//
//				Debug.Log ("LAST CHANCE: " + GameLogic.use.lastChance + " --- nfiles: " + nlifes + " ---- :" + PlayerPrefs.GetInt ("LIFES", 0));
//
					if (Application.internetReachability == NetworkReachability.NotReachable)
					{
					GameObject go = Instantiate (Resources.Load ("Prefabs/EndMenu")) as GameObject;
					}

						else if (nlifes == 0 && PlayerPrefs.GetInt ("LIFES", 0) > 0) {
								GameLogic.use.ToggleRigidBodies (false);
								GameLogic.use.StopLaunch ();
								Time.timeScale = 0f;
								Thief.use._audio.mute = true;
								AudioManager.use._painInTheAss.mute = true;
								AudioManager.use._music.mute = true;
								Siren.use._audio.mute = true;

								GoToPopUp (POPUP_STATES.WINGZ);
						} else if (nlifes == 0 && GameLogic.use.lastChance && hasAds) {
								Debug.Log ("AKi");
								Thief.use._audio.mute = true;
								Siren.use._audio.mute = true;
								AudioManager.use._music.mute = true;
								AudioManager.use._painInTheAss.mute = true;
								GameLogic.use.ToggleRigidBodies (false);
								GameLogic.use.StopLaunch ();
								Time.timeScale = 0f;
								GoToPopUp (POPUP_STATES.AD_COLONY);
						} else if (nlifes == 0 && PlayerPrefs.GetInt ("LIFES", 0) < 0 && !hasAds ) {
								GameLogic.use.ToggleRigidBodies (false);
								GameLogic.use.StopLaunch ();
								Time.timeScale = 0f;
								Thief.use._audio.mute = true;
								Siren.use._audio.mute = true;
								AudioManager.use._painInTheAss.mute = true;
								AudioManager.use._music.mute = true;
								GoToStore ();
						} else if (nlifes == 0 && PlayerPrefs.GetInt ("LIFES", 0) <= 0) {
								GameLogic.use.ToggleRigidBodies (false);
								GameLogic.use.StopLaunch ();
								Time.timeScale = 0f;
								Thief.use._audio.mute = true;
								Siren.use._audio.mute = true;
								AudioManager.use._painInTheAss.mute = true;
								AudioManager.use._music.mute = true;

								GoToStore ();
						}
				}
		}

	public void GoToPopUp(POPUP_STATES popup)
	{
		_popUp = Instantiate (Resources.Load ("Prefabs/PopUp")) as GameObject;
		_popUp.GetComponent<PopUp> ()._state = popup;
		_popUp.GetComponent<PopUp> ().ToggleBackGround ();
	}

	public void GoToStore()
	{
		_store = Instantiate (Resources.Load ("Prefabs/StoreMenu")) as GameObject;
	}

	public void DestroyPauseMenu()
	{
		GameObject.Destroy (_pauseMenu);
	}

	public void DestroyEndMenu()
	{
		if(_endMenu != null) 
		GameObject.Destroy (_endMenu);
	}

	public void UseSlow()
	{
		_powerUp = true;
		_powerUpBar.GetComponent<SpriteSwitcher> ().OtherToggleState (false, true);
		StartCoroutine (Slooooow ());
	}

	public void UseSpeed()
	{
		_powerUp = true;
		_powerUpBar.GetComponent<SpriteSwitcher> ().OtherToggleState (true, false);
		StartCoroutine (Ghost ());
	}

	IEnumerator Slooooow()
	{
		_isSlow = true;
		OnToggle ();
		Time.timeScale = 0.5f;
		GetComponent<AudioSource>().pitch = Time.timeScale;
		_bar.SetActive (true);
		_bar.transform.localScale = _initialScale;
		AudioManager.use._music.pitch = 0.7f;
		AudioManager.use._painInTheAss.pitch =0.7f;
		AudioManager.use._soundEffects.pitch = 0.7f;
		Siren.use._audio.pitch = 0.7f;

		//_bar.GetComponent<iTweenEvent>().
		iTweenEvent.GetEvent (_bar, "Scale").Play ();

		yield return new WaitForSeconds (3f);

		_bar.SetActive (false);

		Time.timeScale = 1f;
		OffToggle();
		_powerUp = false;
		_isSlow = false;
		Thief.use.isPower = false;
		AudioManager.use._music.pitch = 1f;
		AudioManager.use._painInTheAss.pitch =1f;
		AudioManager.use._soundEffects.pitch = 1f;
		Siren.use._audio.pitch = 1f;
		_powerUpBar.GetComponent<SpriteSwitcher> ().OtherToggleState (false, false);
	}

	IEnumerator Ghost()
	{
		Thief.use.GetComponent<BoxCollider> ().GetComponent<Collider>().enabled = false;
		iTween[] tweens = Thief.use._playa.GetComponents<iTween> ();

		foreach (iTween tween in tweens) {

			if(tween.name.Contains("Fade"))
			{
				tween.time = 0;
				tween.SendMessage("Update");
				break;
			}
		}

		_bar.SetActive (true);
		_bar.transform.localScale = _initialScale;
		
		iTweenEvent.GetEvent (_bar, "Scale").Play ();


		Thief.use.BoostOn ();
		
		yield return new WaitForSeconds (3f);

		_bar.SetActive (false);

		Thief.use.BoostOff ();

		
		//Thief.use.GetComponent<BoxCollider> ().collider.enabled = true;
		_powerUpBar.GetComponent<SpriteSwitcher> ().OtherToggleState (false, false);
		_powerUp = false;
	}

	private void SwapTextSize()
	{
		if (_iscore.ToString ().Length >= 9)
			_score.gameObject.GetComponent<TextMesh> ().fontSize = 31;

		else if (_iscore.ToString ().Length > 6 && _iscore.ToString ().Length < 8)
			_score.gameObject.GetComponent<TextMesh> ().fontSize = 45;

		else if (_iscore.ToString ().Length == 8)
			_score.gameObject.GetComponent<TextMesh> ().fontSize = 35;
	}

	public void StopPowerUps()
	{
		StopCoroutine ("Ghost");
		StopCoroutine ("Slooooow");


		OffToggle();
		_powerUp = false;
		_isSlow = false;
		Thief.use.isPower = false;
		AudioManager.use._music.pitch = 1f;
		AudioManager.use._painInTheAss.pitch =1f;
		AudioManager.use._soundEffects.pitch = 1f;
		Siren.use._audio.pitch = 1f;
		_powerUpBar.GetComponent<SpriteSwitcher> ().OtherToggleState (false, false);
	}
}
