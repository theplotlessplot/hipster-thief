﻿using UnityEngine;
using System.Collections;

public class LoadingScreenManager : MonoBehaviour {

	public GameObject LoadingScreen;

	public GameObject SplashScreen;

	public GameObject MainMenu;

	private double lastInterval = 0;

	void Awake()
	{

		if (GameObject.Find ("AudioManager(Clone)") == null) 
		{
			GameObject go = Instantiate (Resources.Load ("Prefabs/AudioManager")) as GameObject;
		}

		if (GameObject.Find ("InApp(Clone)") == null) 
		{
			GameObject go = Instantiate (Resources.Load ("Prefabs/InApp")) as GameObject;
		}

	}
	// Use this for initialization
	void Start () 
	{ 

		Debug.Log ("TIME : " + Time.realtimeSinceStartup);
		if (Time.realtimeSinceStartup <=  5)
			StartCoroutine ("WaitForIt");
		else
			OpenMenu ();

		lastInterval = Time.realtimeSinceStartup;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public IEnumerator WaitForIt()
	{

		yield return new WaitForSeconds(2.0f);
		SplashScreen.SetActive (false);
		LoadingScreen.SetActive (true);

		yield return new WaitForSeconds(3.0f);
		LoadingScreen.SetActive (false);
		
		if (MainMenu != null)
			MainMenu.SetActive (true);
	}

	public void OpenMenu()
	{
		SplashScreen.SetActive (false);
		LoadingScreen.SetActive (false);

		if (MainMenu != null)
			MainMenu.SetActive (true);

	}

	public IEnumerator GetReady()
	{
		yield return new WaitForSeconds(2.0f);
		AudioManager.use.StopAll ();
		Application.LoadLevel("Game");
	}
	
	public IEnumerator GetExit()
	{
		yield return new WaitForSeconds(2.0f);
		Application.LoadLevel("Menu");
	}
}
