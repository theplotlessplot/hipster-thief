﻿using UnityEngine;
using System.Collections;

public class StoreMenu : MonoBehaviour {
	
	
	public TextMesh _priceOne, _priceTwo, _priceThree, _lifesText;
	
	public float fromValue = 0;
	
	// Use this for initialization
	void Start () 
	{
		_priceOne.GetComponent<Renderer>().sortingOrder = 6;
		_priceTwo.GetComponent<Renderer>().sortingOrder = 6;
		_priceThree.GetComponent<Renderer>().sortingOrder = 6;
		_lifesText.GetComponent<Renderer>().sortingOrder = 6;
		
		_lifesText.text = PlayerPrefs.GetInt ("LIFES", 0) >= 0 ? PlayerPrefs.GetInt ("LIFES", 0).ToString () : (0).ToString();
		
		if (!Application.isEditor) 
		{
			
			#if UNITY_IPHONE
			//	_priceOne.text = InApp.use.GetPriceSKU (InApp.SKU_5_LIFES);
			
			//_priceTwo.text = InApp.use.GetPriceSKU (InApp.SKU_15_LIFES);
			
			//_priceThree.text = InApp.use.GetPriceSKU (InApp.SKU_25_LIFES);
			
			_priceOne.text = InApp.use.GetPriceSKU ("VintagePack");
			
			_priceTwo.text = InApp.use.GetPriceSKU ("YoloPack");
			
			_priceThree.text = InApp.use.GetPriceSKU ("SwagPack");
			#endif
			
			#if UNITY_ANDROID
			//_priceOne.text = InApp.use.GetPriceSKU ("vintagepack");
			
			//_priceTwo.text = InApp.use.GetPriceSKU ("yolopack");
			
			//_priceThree.text = InApp.use.GetPriceSKU ("swagpack");
			#endif
			
		}
	}
	
	// Update is called once per frame
	void Update () { }
	
	public void BackToMenu() { Destroy (); }
	
	public void BackToGameOver() 
	{
		Debug.Log ("SAir da Store");
		Destroy (); 
	}
	
	public void Destroy()
	{
		Debug.Log ("Destroy Store");
		if (Application.loadedLevelName.CompareTo ("Menu") == 0) 
		{
			Debug.Log ("Destroy Store Menu");
			GameObject.Find ("MainMenu").SendMessage ("EnableInput", SendMessageOptions.DontRequireReceiver);
			GameObject.Find ("MainMenu").SendMessage ("ToggleLogo", true, SendMessageOptions.DontRequireReceiver);
		}
		
		Close ();
		
		iTweenEvent.GetEvent (this.gameObject, "MoveDown").Play ();
	}
	
	public void Close()
	{
		
		Debug.Log ("Close Loja");
		if (Application.loadedLevelName.CompareTo ("Menu") != 0) 
		{
			if(PlayerPrefs.GetInt("LIFES") > 0){
				GameObject _popUp = Instantiate (Resources.Load ("Prefabs/PopUp")) as GameObject;
				_popUp.GetComponent<PopUp> ()._state = POPUP_STATES.WINGZ;
				_popUp.GetComponent<PopUp> ().ToggleBackGround ();
			}
			
			//Debug.Log ("Instantiar Game OVer");
			else if(Hud.use._endMenu == null)
			{
				GameObject go = Instantiate (Resources.Load ("Prefabs/EndMenu")) as GameObject;
			}
			
		}
		
		GameObject.Destroy (this.gameObject);
	}
	
	private void Up()
	{
		iTween.ValueTo( gameObject, iTween.Hash(
			"from" , this.transform.localPosition.y,
			"to" , -0.19f,
			"time" , 1,
			"onUpdate", "updateFromValue",
			"easeType", iTween.EaseType.easeOutSine
			));
	}
	
	private void Down()
	{
		iTween.ValueTo( gameObject, iTween.Hash(
			"from" , this.transform.localPosition.y,
			"to" , -3f,
			"time" , 1,
			"onUpdate", "updateFromValue",
			"easeType", iTween.EaseType.easeOutSine,
			"onComplete", "Destroy"
			));
	}
	
	public void updateFromValue( float newValue )
	{
		fromValue = newValue;
		this.gameObject.GetComponent<AnchorMultiResolution>().yTransform = fromValue;
	}
	
	public void BuyItemOne()
	{
		
		
		//		FuseAPI.RegisterEvent ("Buy_Item_One");
		#if UNITY_IPHONE
		//OpenIAB.purchaseProduct ("921175344");
		OpenIAB.purchaseProduct ("VintagePack");
		#elif UNITY_ANDROID
		//OpenIAB.purchaseProduct ("vintagepack");
		#endif
		_lifesText.text = PlayerPrefs.GetInt ("LIFES", 0).ToString();
	}
	
	public void BuyItemTwo()
	{
		//		FuseAPI.RegisterEvent ("Buy_Item_Two");
		
		#if UNITY_IPHONE
		OpenIAB.purchaseProduct ("YoloPack");
		//OpenIAB.purchaseProduct ("921175593");
		#elif UNITY_ANDROID
		//OpenIAB.purchaseProduct ("yolopack");
		#endif
		_lifesText.text = PlayerPrefs.GetInt ("LIFES", 0).ToString();
	}
	
	public void BuyItemThree()
	{
		//		FuseAPI.RegisterEvent ("Buy_Item_Three");
		#if UNITY_IPHONE
		OpenIAB.purchaseProduct ("SwagPack");
		//OpenIAB.purchaseProduct ("921175603");
		#elif UNITY_ANDROID
		//OpenIAB.purchaseProduct ("swagpack");
		#endif
		_lifesText.text = PlayerPrefs.GetInt ("LIFES", 0).ToString();
	}
}
