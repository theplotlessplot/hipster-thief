﻿using UnityEngine;
using System.Collections;

public class PauseMenu : MonoBehaviour 
{
	public AudioSource _music;

	private bool _sfxOn;

	public void RestartGame()
	{
		Time.timeScale = 1f;
		/*GameLogic.use.pause = false;
		GameLogic.use.actualLevel = 0;
		Hud.use.iscore = 0;
		Hud.use.nlifes = 3;
		Hud.use._lifes.text = Hud.use.nlifes.ToString ();
		GameObject.Destroy(GameLogic.use._trashCan);
		GameObject.Find ("Hud").GetComponent<Hud>().DestroyPauseMenu ();*/
		Application.LoadLevel ("Game");
	}

	public void ResumeGame()
	{
		Time.timeScale = 1f;
		this.gameObject.SetActive(false);
		GameLogic.use.pause = false;
		GameObject.Find ("Hud").GetComponent<Hud>().DestroyPauseMenu ();
		if(Hud.use._popUp)
			Hud.use._bar.GetComponent<iTween> ().enabled = true;
	}

	public void GoToMenu()
	{
		AudioManager.use.StopAll ();
		Application.LoadLevel ("Menu");
		Time.timeScale = 1f;
	}

	public void ToggleSfx()
	{
		_sfxOn = !_sfxOn;
		AudioManager.use._On = !AudioManager.use._On;
		PlayerPrefsX.SetBool ("SFX", _sfxOn);
		
		BroadcastMessage ("ToggleState", _sfxOn, SendMessageOptions.DontRequireReceiver);

		Thief.use.setAudio (AudioManager.use._On);
		Siren.use.setAudio (AudioManager.use._On);
		AudioManager.use.MusicToggle(AudioManager.use._On);
	}

	// Use this for initialization
	void Start () 
	{
		if(Hud.use._powerUp)
			Hud.use._bar.GetComponent<iTween> ().enabled = false;
		_music = Hud.use._music;

		_sfxOn = PlayerPrefsX.GetBool ("SFX", true);
		
		BroadcastMessage ("ToggleState", _sfxOn, SendMessageOptions.DontRequireReceiver);
	}
	
	// Update is called once per frame
	void Update () { }
}
