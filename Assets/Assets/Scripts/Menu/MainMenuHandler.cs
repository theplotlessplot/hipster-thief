﻿using UnityEngine;
using System.Collections;

public class MainMenuHandler : MonoBehaviour {

	public void TapPlayButton()
	{
		AudioManager.use.PlayButtonSound ();
		SendMessage("GoToGame", SendMessageOptions.DontRequireReceiver);
	}

	public void TapSoundButton()
	{
		AudioManager.use.PlayButtonSound ();
		SendMessage("ToggleSfx", SendMessageOptions.DontRequireReceiver);
	}

	public void TapLeadButton()
	{
		AudioManager.use.PlayButtonSound ();
		SendMessage("GoToLead", SendMessageOptions.DontRequireReceiver);
	}

	public void TapTrophieButton()
	{
		AudioManager.use.PlayButtonSound ();
		SendMessage("GoToTrophies", SendMessageOptions.DontRequireReceiver);
	}

	public void TapMoneyButton()
	{
		AudioManager.use.PlayButtonSound ();
		SendMessage("Money", SendMessageOptions.DontRequireReceiver);
	}
	public void TapStoreButton()
	{
		AudioManager.use.PlayButtonSound ();
		SendMessage("GoToStore", SendMessageOptions.DontRequireReceiver);
	}

	// Use this for initialization
	void Start () { }
	
	// Update is called once per frame
	void Update () { }
}
