using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour {

	private string		version;
	private string		appId;
	private string		zoneId1;
	private string		zoneId2;
	private string		v4vcId;
	private bool _sfxOn;

	public AudioSource _music;
	public AudioSource _motoSound;

	public TextMesh _score;
	public TextMesh _scoreLabel;


	public GameObject _logo;

	public GameObject _hipster;
	public float fromValue = 0;

	public GameObject _addManager;

	private bool _ava;

	public GameObject _internetConnection;
	public GameObject _store;

	public void ToggleSfx()
	{
		if (_internetConnection != null)
			return;

		_sfxOn = !_sfxOn;
		PlayerPrefsX.SetBool ("SFX", _sfxOn);

		BroadcastMessage ("ToggleState", _sfxOn, SendMessageOptions.DontRequireReceiver);

		AudioManager.use.ToggleSound(_sfxOn);

		AudioManager.use.PlayMusicMenu ();
		AudioManager.use.PlayMotoSound ();
	}

	public void GoToLead()
	{
		if (_internetConnection != null)
			return;
//		FuseAPI.RegisterEvent ("Leaderboard_Pressed");


#if UNITY_IPHONE
		GameCenterSingleton.Instance.ReportScore((long)PlayerPrefs.GetInt ("HIGH_SCORE", 0), "HT_Leaderboard");
		GameCenterSingleton.Instance.ShowAchievementUI();
#endif

#if UNITY_ANDROID
		if (!Social.localUser.authenticated) {
			// Authenticate
			Social.localUser.Authenticate((bool success) => {
				Social.ShowLeaderboardUI();

			});
		} else {
			// Sign out!
			Social.ReportScore((long)PlayerPrefs.GetInt ("HIGH_SCORE", 0),"CgkIqoCS9doCEAIQDA", null);
			Social.ShowLeaderboardUI();
			//((GooglePlayGames.PlayGamesPlatform) Social.Active).SignOut();
		}
#endif

			
	}

	public void GoToTrophies()
	{
		if (_internetConnection != null)
			return;
//		FuseAPI.RegisterEvent ("Trophies_Pressed");

		#if UNITY_IPHONE
		GameCenterSingleton.Instance.ReportScore((long)PlayerPrefs.GetInt ("HIGH_SCORE", 0), "HT_Leaderboard");
		GameCenterSingleton.Instance.ShowAchievementUI();
		#endif


#if UNITY_ANDROID
		if (!Social.localUser.authenticated) {
			// Authenticate
			Social.localUser.Authenticate((bool success) => {

				Social.ShowAchievementsUI();

			});
		} else {
			// Sign out!
			Social.ShowAchievementsUI();
			//((GooglePlayGames.PlayGamesPlatform) Social.Active).SignOut();
		}
#endif
	}

	public void GoToGame()
	{
		if (_internetConnection != null)
						return;
//		FuseAPI.RegisterEvent ("Play_Pressed");

		AudioManager.use.PlayMotoSound ();
		iTweenEvent.GetEvent(_hipster, "MoveHipster").Play();
	
		GameObject loadingScreens = GameObject.Find ("LoadingScreenManager");
		
		StartCoroutine (loadingScreens.GetComponent<LoadingScreenManager>().GetReady());
	}

	private void GoToRight()
	{
		iTween.ValueTo( this.gameObject, iTween.Hash(
			"from" , _hipster.transform.localPosition.x,
			"to" , 2f,
			"time" , 1,
			"onUpdate", "updateFromValue",
			"easeType", iTween.EaseType.easeOutSine
			));
	}

	public void updateFromValue( float newValue )
	{
		fromValue = newValue;
		_hipster.gameObject.GetComponent<AnchorMultiResolution>().xTransform = fromValue;
	}

	public void GoToStore()
	{
		if (Application.internetReachability == NetworkReachability.NotReachable)
		{
			_internetConnection = Instantiate (Resources.Load ("Prefabs/InternetPopUp")) as GameObject;
			_internetConnection.GetComponent<InternetPopUp>()._mainMenu = this.gameObject;
			return;
		}

//		FuseAPI.RegisterEvent ("Store_Pressed");
		DisableInput ();
		ToggleLogo (false);

		_store = Instantiate (Resources.Load ("Prefabs/StoreMenu")) as GameObject;
	}

	public void ToggleLogo(bool flag)
	{
		_logo.SetActive (flag);
		_score.gameObject.SetActive (flag);
		_scoreLabel.gameObject.SetActive (flag);
	}

	public void EnableInput()
	{
		foreach (Transform t in this.gameObject.transform)
			if(t.GetComponent<ExampleMenuItem> () != null)
				t.GetComponent<ExampleMenuItem> ().listeningForInput = true;
	}

	public void DisableInput()
	{
		foreach (Transform t in this.gameObject.transform)
			if(t.GetComponent<ExampleMenuItem> () != null)
				t.GetComponent<ExampleMenuItem> ().listeningForInput = false;
	}

	void Awake()
	{
		AudioManager.use.StopAll ();

		//if (Time.realtimeSinceStartup <= 6) {

			//			FB.Init (CallFBLogin);

			//	}


		#if UNITY_ANDROID
		//GooglePlayGames.PlayGamesPlatform.Activate();
		#endif

		FB.Init (Init);
		//AudioManager.use.StopMusicMenu ();
		//AudioManager.use.StopMotoSound ();
	}

	public void Init()
	{
		Debug.Log ("FB INIT");
	}

	public void CallFBLogin()
	{
		Debug.Log ("ASDASDASD");
		FB.Login("email,publish_actions", LoginCallback);
	}
	
	void LoginCallback(FBResult result)
	{     
		if (FB.IsLoggedIn)                                                                     
		{         
			Debug.Log ("Fiz logado");                                                                             
		} 
	}

	// Use this for initialization
	void Start () {

	//	PlayerPrefs.DeleteAll ();
		Time.timeScale = 1f;

	//	if (GameObject.Find ("AddManager") == null) 
	//	{
	//		_addManager = Instantiate (Resources.Load ("Prefabs/AddManager")) as GameObject;
	//		_addManager.name = "AddManager";
	//	}


		_sfxOn = PlayerPrefsX.GetBool ("SFX", true);
		_score.text = PlayerPrefs.GetInt ("HIGH_SCORE", 0).ToString ();
		
		BroadcastMessage ("ToggleState", _sfxOn, SendMessageOptions.DontRequireReceiver);
		AudioManager.use.ToggleSound (_sfxOn);

		GameCenterSingleton.Instance.Initialize ();

		_score.GetComponent<Renderer>().sortingLayerName= "Score";
		_score.GetComponent<Renderer>().sortingOrder = 0;

		StartCoroutine (Delay ());
	}
	
	IEnumerator Delay()
	{
		yield return new WaitForSeconds (1f);
		
		AudioManager.use.PlayMusicMenu ();
		AudioManager.use.PlayMotoSound ();


		if (Application.internetReachability == NetworkReachability.NotReachable) {
						_internetConnection = Instantiate (Resources.Load ("Prefabs/InternetPopUp")) as GameObject;
			_internetConnection.GetComponent<InternetPopUp>()._mainMenu = this.gameObject;
				}
		
		//_motoSound.Play ();
	}

	public void OnApplicationQuit() {
		PlayerPrefsX.SetBool ("FBINIT", false);
	}

	// Update is called once per frame
	void Update () {
	
	}
}
