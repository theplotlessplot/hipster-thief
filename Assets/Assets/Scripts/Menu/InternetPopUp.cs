﻿using UnityEngine;
using System.Collections;

public class InternetPopUp : MonoBehaviour {

	public GameObject _mainMenu;


	public void BackToMenu() { Destroy (); }

	public void Destroy()
	{
		_mainMenu.GetComponent<MainMenu> ()._internetConnection = null;	
		GameObject.Destroy (this.gameObject);
	}
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
