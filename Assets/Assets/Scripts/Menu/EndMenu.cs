﻿using UnityEngine;
using System.Collections;

public class EndMenu : MonoBehaviour {

	public TextMesh _bestDistance;

	public TextMesh _distance;

	public void Share()
	{
				if (!FB.IsInitialized) {
						FB.Init (CallFBLogin);
				} else {
						if (FB.IsLoggedIn) {
								Debug.Log ("Akisdasdsa");

				FB.Feed (linkDescription: "",               
				         picture: "http://www.storiesstudio.com/jogos/facebook/ht.jpg",                                                     
				         linkName: "I'm sooo Hipster! Beat Me!       " + Hud.use.iscore + " m !!!",                                                                 
			         link: "http://play.google.com/store/apps/details?id=com.StoriesStudio.HipsterThief",
				         linkCaption:"" );     
						} else
								CallFBLogin ();
			}
	}



	public void CallFBLogin()
	{
		FB.Login("email,publish_actions", LoginCallback);
	}
	
	void LoginCallback(FBResult result)
	{     
		if (FB.IsLoggedIn)                                                                     
		{         
			FB.Feed (linkDescription: "",               
			         picture: "http://www.storiesstudio.com/jogos/facebook/ht.jpg",                                                     
			         linkName: "I'm sooo Hipster! Beat Me!         "+ Hud.use.iscore + " m !!! Almost there!",                                                                 
			         link: "http://play.google.com/store/apps/details?id=com.StoriesStudio.HipsterThief",
					linkCaption:"" );                                                                 
		} 
	}

	// Use this for initialization
	void Start () 
	{
		AudioManager.use._music.mute = true;
		AudioManager.use._soundEffects.mute = true;
		GameLogic.use.pause = true;

		if (AudioManager.use._On)
				Siren.use._audio.mute = false;

		int bestDistance = PlayerPrefs.GetInt ("HIGH_SCORE", 0);

		Hud.use._endMenu = this.gameObject;

		_bestDistance.GetComponent<Renderer>().sortingOrder = 6;
		_distance.GetComponent<Renderer>().sortingOrder = 6;

		if (Hud.use.iscore > bestDistance) 
		{
			_bestDistance.text = Hud.use.iscore.ToString ();
			_distance.text = Hud.use.iscore.ToString (); 
			PlayerPrefs.SetInt("HIGH_SCORE", Hud.use.iscore);

			#if UNITY_IPHONE
			GameCenterSingleton.Instance.ReportScore((long)Hud.use.iscore, "HT_Leaderboard");
			#endif


			#if UNITY_ANDROID
			Social.ReportScore((long)Hud.use.iscore,"CgkIqoCS9doCEAIQDA", null);
			#endif
		} 
		else 
		{
			_bestDistance.text = bestDistance.ToString ();
			_distance.text = Hud.use.iscore.ToString (); 
		}
	}
	
	// Update is called once per frame
	void Update () { }
	public void RestartGame()
	{
	/*	Time.timeScale = 1f;
		GameObject.Find ("Hud").GetComponent<Hud>().DestroyEndMenu ();
		Hud.use.StopPowerUps ();
		GameLogic.use.actualLevel = 0;
		Hud.use.iscore = 0;
		Hud.use._score.text = "0";
		Hud.use.nlifes = 3;
		//Thief.use.DisableAllSigns ();
		Thief.use.DisableSign ();
		Thief.use.RestartAlpha ();
		iTweenEvent.GetEvent (Thief.use._spriteRenderer.gameObject, "Fade").Stop ();
		GameLogic.use.pause = false;
		Hud.use._lifes.text = Hud.use.nlifes.ToString ();
		GameObject.Destroy(GameLogic.use._trashCan);*/
		Time.timeScale = 1f;
		Application.LoadLevel ("Game");
	}
	
	public void GoToMenu()
	{
		Time.timeScale = 1f;
		Application.LoadLevel ("Menu");
	}

	public void OnApplicationQuit() {
		PlayerPrefsX.SetBool ("FBINIT", false);
	}
}
