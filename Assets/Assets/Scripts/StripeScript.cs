﻿using UnityEngine;
using System.Collections;

public class StripeScript : MonoBehaviour {


	public Vector3 _initialPosition;
	public float _initialMass;
	public float _initialDrag;
	private GameLogic gl;

	// Use this for initialization
	void Awake () {

		_initialPosition = new Vector3 (this.transform.localPosition.x, 0f, this.transform.localPosition.z);
		_initialDrag = this.GetComponent<Rigidbody> ().drag;
		_initialMass = this.GetComponent<Rigidbody> ().mass;
		;
		
	}

	void Start()
	{
		gl = Hud.use.GetComponent<GameLogic> ();
		}
	
	// Update is called once per frame
	void Update () {
//		this.gameObject.GetComponent<Rigidbody>().drag = gl.levels[gl.actualLevel]._drag * 1.5f;
		this.gameObject.GetComponent<Rigidbody>().drag = GameLogic.use._gdrag * 1.5f;
	}

	public void OnTriggerEnter(Collider other)
	{
		if (other.transform.tag.CompareTo ("OutOfBounds") == 0) 
		{
			this.transform.localPosition = _initialPosition;
//			this.gameObject.GetComponent<Rigidbody>().drag = gl.levels[gl.actualLevel]._drag;

		}
	}
}