﻿using UnityEngine;
using System.Collections;

public class SpriteButton : ButtonScript 
{
	public GameObject[] sprites = new GameObject[2];

	private Vector3 _initialScale;
	private Vector3 _pressedScale;

	public void Awake()
	{
		_initialScale = this.gameObject.transform.localScale;
		
		Vector3 temp = new Vector3 (this.gameObject.transform.localScale.x * 0.9f, this.gameObject.transform.localScale.y * 0.9f, this.gameObject.transform.localScale.z * 0.9f);
		
		_pressedScale = temp;
	}

	public override void Focus () 
	{ 
		sprites[1].SetActive(true);
		sprites[0].SetActive(false);
		this.gameObject.transform.localScale = _pressedScale;
	}

	public override void UnFocus()
	{
		sprites[0].SetActive(true);
		sprites[1].SetActive(false);
		this.gameObject.transform.localScale = _initialScale;
	}
}