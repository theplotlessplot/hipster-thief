﻿using UnityEngine;
using System.Collections;

public class SimpleButton : ButtonScript 
{
	public TextMesh _text;

	public Color _textColorPressed;

	public Color _textColorUnPressed;

	private Vector3 _initialScale;
	private Vector3 _pressedScale;

	void Awake()
	{
		_initialScale = this.gameObject.transform.localScale;

		Vector3 temp = new Vector3 (this.gameObject.transform.localScale.x * 0.9f, this.gameObject.transform.localScale.y * 0.9f, this.gameObject.transform.localScale.z * 0.9f);

		_pressedScale = temp;
	}

	public override void Focus() 
	{ 
		Debug.Log ("FOCUS");
		//this.gameObject.renderer.enabled=true; 

		this.gameObject.transform.localScale = _pressedScale;
//		_text.renderer.material.color = _textColorPressed;
	}

	public override void UnFocus() 
	{ 
		this.gameObject.transform.localScale = _initialScale;
	//	_text.renderer.material.color = _textColorUnPressed;
	//	this.gameObject.renderer.enabled=false; 
	}
}