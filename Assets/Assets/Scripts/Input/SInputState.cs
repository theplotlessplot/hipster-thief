using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum EInputState
{
	E_IS_TOUCH,
	E_IS_KEYBOARD,	
	E_IS_MOUSE,
	E_IS_JOYSTICK,
	E_IS_GAMEPAD
}

/*
 * 
 * Classes for handling input - one class per input method
 * Minimum requirements for adding an input method:
 *  - override at least one event - and fire it accordingly
 *  - register class with SInputManager, in OnStart
 * 	- unregister class from SInputManager, on OnExit
 * 
 * */

public class SInputState : MonoBehaviour {
	
	public virtual event SInputManager.InputPosition InputStart;
	public virtual event SInputManager.InputPosition InputRelease;
	public virtual event SInputManager.InputPosition InputUpdate;
	
	public virtual event SInputManager.InputAxis AxisStart;
	public virtual event SInputManager.InputAxis AxisRelease;
	public virtual event SInputManager.InputAxis AxisUpdate;
	
	public virtual event SInputManager.InputVector VectorUpdate;
	
	public virtual void Update()
	{
		
	}
	
	public virtual void OnStart()
	{
		
	}
	
	public virtual void OnExit()
	{
		
	}
	
	
}

public class STouchState : SInputState
{
	
	public override event SInputManager.InputPosition InputStart;
	public override event SInputManager.InputPosition InputRelease;
	public override event SInputManager.InputPosition InputUpdate;
	
	public SFinger[] fingers;
	
	public override void OnStart()
	{		
		SInputManager.Register(this);
		
		fingers = new SFinger[5];
		this.clearFingers();

	}	
	
	private void clearFingers()
	{
		for(int i = 0; i < fingers.Length; i++)
		{
			if(fingers[i] == null)
			{
				fingers[i] = new SFinger(-1, Vector2.zero);
			}
			else
			{
				if(fingers[i].id != -1)
				{
					// finger was lifted	
					if(InputRelease != null)
						InputRelease(fingers[i].id, fingers[i].position);
				}
			}
			fingers[i].id = -1;
		}	
	}
	
	public override void Update()
	{
		if(Input.touchCount > 0 && numFingersInArray() == 0)
		{
			for(int i=0; i < Input.touchCount; i++)
			{
				this.InitiateFinger(i, Input.touches[0].position);
			}
		}
			
		if(Input.touchCount > 0)
		{
			this.updateFingers();	
		}
		
		if(Input.touchCount == 0)
		{
			this.clearFingers();	
		}
	}
	
	private int numFingersInArray()
	{
		int count = 0;
		foreach(SFinger f in fingers)
		{
			if(f.id != -1)
				count++;
		}
		return count;
	}
	 
	private void updateFingers()
	{
		foreach(SFinger f in fingers)
		{
			if(f.id != -1)
			{
				bool found = false;
				
				foreach(Touch t in Input.touches)
				{
					if(f.id == t.fingerId)
					{
						found = true;
						if(f.id >= 0)
						{
							f.Update(t.position);
							if(InputUpdate != null)
								InputUpdate(f.id, f.position);
						}
					}
				}
				//remove fingers that are not touching anymore
				if(!found)
				{
					if(InputRelease != null)
						InputRelease(f.id, f.position);
					f.id = -1;	
				}
			}
		}
		//check for new fingers
		foreach(Touch t in Input.touches)
		{
			bool foundF = false;
			foreach(SFinger f in fingers)
			{
				if(t.fingerId == f.id)
				{
					foundF = true;	
				}
			}
			
			if(!foundF)
			{
				this.InitiateFinger(t.fingerId, t.position);	
				
			}
		}	
		
	}	
	
	private void InitiateFinger(int fingerIndex, Vector2 position)
	{
		if(fingerIndex > fingers.Length -1)
			return;
		fingers[fingerIndex] = new SFinger(fingerIndex, position);
		if(InputStart != null)
			InputStart(fingerIndex, position);
	}
	
	public override void OnExit()
	{
		this.clearFingers();	
		SInputManager.Unregister(this);
	}
	
}

public class SArduinoPotState : SInputState
{
	public override event SInputManager.InputVector VectorUpdate;
	
	float potVal = 0;
	
	public override void Update()
	{
	//	if(SerialPortTest.strIn != "")
	//	{
	//		potVal = float.Parse(SerialPortTest.strIn);
	//	}
	//	VectorUpdate(convertFloatToVector(potVal));	
	}
	
	public override void OnStart()
	{
		SInputManager.Register(this);
	}
	
	public override void OnExit()
	{
		SInputManager.Unregister(this);	
	}
	
	private Vector2 convertFloatToVector(float val) // converting pot val to vector -> 0 (closed) = full left, 512 (middle) = full front, 1023 (open) = full right
	{
		if(val == 0)
			return Vector3.left;
		else if(val == 1023)
			return Vector2.right;
		else if(val < 512)
		{
			float lVal = val / 512;
			float yVal = lVal;
			
			return new Vector2(lVal-1, yVal);
		}
		else if(val >= 512)
		{
			float lVal = val / 512 - 1;
			float yVal = 1 - lVal;
			
			return new Vector2(lVal, yVal);
		}
		else if(val == 512)
			return Vector2.up;
		
		return Vector2.zero;
	}
}


public class SGamePadState : SInputState
{
	private List<Axis> axes = new List<Axis>();
	
	private List<Axis> usedAxes = new List<Axis>();
	
	public override event SInputManager.InputAxis AxisStart;
	public override event SInputManager.InputAxis AxisRelease;
	public override event SInputManager.InputAxis AxisUpdate;
		
	private bool throwStart = false;
	private bool throwRelease = false;
	
	public override void Update()
	{
/**
		foreach(Axis a in axes)
		{
			if(a.active)
			{
				if(MOGAListener.GetAxis(a.name) == 0)
				{
					a.SetActive(false);
					a.SetVal(0);
					usedAxes.Remove(a);
					throwRelease = true;
				}
				else
				{
					a.SetVal(MOGAListener.GetAxis(a.name));	
				}
			}
			else
			{
				if(MOGAListener.GetAxis(a.name) != 0)
				{
					a.SetActive(true);
					a.SetVal(MOGAListener.GetAxis(a.name));
					usedAxes.Add(a);
					throwStart = true;
				}
			}
		}
		
		if(throwStart)
		{
			throwStart = false;
			AxisStart(usedAxes);
		}
		
		if(throwRelease)
		{
			throwRelease = false;
			AxisRelease(usedAxes);
		}
		
		if(usedAxes.Count > 0)
			AxisUpdate(usedAxes);		
		
#endif
		*/	
	}
	
	public override void OnStart()
	{
		axes.Clear();
		
		SInputManager.Register(this);
		
		// register axes here - hardcoded for now
		axes.Add(new Axis("Horizontal"));
		axes.Add(new Axis("Vertical"));
		axes.Add(new Axis("Jump"));
		axes.Add(new Axis("Escape"));
	}
	
	public override void OnExit()
	{
		SInputManager.Unregister(this);
	}
	
}

public class SMouseState : SInputState
{
	public override event SInputManager.InputPosition InputStart;
	public override event SInputManager.InputPosition InputRelease;
	public override event SInputManager.InputPosition InputUpdate;
	
	public override void Update()
	{
		if(Input.GetMouseButtonDown(0))
		{
			if(InputStart != null)
				InputStart(0, Input.mousePosition);
		}
		
		if(Input.GetMouseButtonUp(0))
		{
			if(InputRelease != null)
				InputRelease(0, Input.mousePosition);
		}
		
		if(Input.GetMouseButton(0))
		{
			if(InputUpdate != null)
				InputUpdate(0, Input.mousePosition);
		}
	}
	
	public override void OnStart()
	{
		SInputManager.Register(this);
	}
	
	public override void OnExit()
	{
		SInputManager.Unregister(this);	
	}
}

public class SKeyboardState : SInputState
{
	private List<Axis> axes = new List<Axis>();
	
	private List<Axis> usedAxes = new List<Axis>();
	
	public override event SInputManager.InputAxis AxisStart;
	public override event SInputManager.InputAxis AxisRelease;
	public override event SInputManager.InputAxis AxisUpdate;
		
	private bool throwStart = false;
	private bool throwRelease = false;
	
	public override void Update()
	{
		foreach(Axis a in axes)
		{
			if(a.active)
			{
				if(Input.GetAxisRaw(a.name) == 0)
				{
					a.SetActive(false);
					a.SetVal(0);
					usedAxes.Remove(a);
					throwRelease = true;
				}
				else
				{
					a.SetVal(Input.GetAxisRaw(a.name));	
				}
			}
			else
			{
				if(Input.GetAxisRaw(a.name) != 0)
				{
					a.SetActive(true);
					a.SetVal(Input.GetAxisRaw(a.name));
					usedAxes.Add(a);
					throwStart = true;
				}
			}
		}
		
		if(throwStart)
		{
			throwStart = false;
			AxisStart(usedAxes);
		}
		
		if(throwRelease)
		{
			throwRelease = false;
			AxisRelease(usedAxes);
		}
		
		if(usedAxes.Count > 0)
			AxisUpdate(usedAxes);			
			
	}
	
	public override void OnStart()
	{
		axes.Clear();
		
		SInputManager.Register(this);
		
		// register axes here - hardcoded for now
		axes.Add(new Axis("Horizontal"));
		axes.Add(new Axis("Vertical"));
		axes.Add(new Axis("Jump"));
		axes.Add(new Axis("Escape"));
		axes.Add(new Axis("SecondaryHorizontal"));
		axes.Add(new Axis("SecondaryVertical"));
	}
	
	public override void OnExit()
	{
		SInputManager.Unregister(this);
	}
}

[System.Serializable]
public class Axis
{	
	public string name;
	public bool active = false;	
	public float val = 0;
	
	public Axis(string name)
	{
		this.name = name;
	}
	
	public void SetActive(bool a)
	{
		this.active = a;	
	}
	
	public void SetVal(float val)
	{
		this.val = val;	
	}
}