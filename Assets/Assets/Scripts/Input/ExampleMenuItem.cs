/*
 * 
 * 	EXAMPLE MENU ITEM
 * 
 *  use this to set focus behaviours and check for tap collisions
 * 
 * */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum EDirection {
	E_D_LEFT,
	E_D_RIGHT,
	E_D_UP,
	E_D_DOWN
}

public class ExampleMenuItem : MonoBehaviour {
	
	public bool listeningForInput = true;
	public Transform myTransform;
	private MeshRenderer thisRenderer;
	private SpriteRenderer thisRendererS;
	public EFocusState focusState;
	
	public List<SFocusState> focusStates = new List<SFocusState>();
	
	public ExampleMenuItem[] Neighbours = new ExampleMenuItem[4];
	
	public static ExampleMenuItem use;
	
	public void DisableInput() { listeningForInput = false; }
	public void EnableInput()  { listeningForInput = true; }
	
	// Use this for initialization
	void Start () 
	{
		myTransform = transform;
		thisRenderer = gameObject.GetComponent<MeshRenderer>();
		if (thisRenderer == null)
			thisRendererS = gameObject.GetComponent<SpriteRenderer>();
		ExampleMenuController.RegisterItem(this);
		
		PressedState pressedState = new PressedState();
		focusStates.Add(pressedState);
		
		foreach(SFocusState f in focusStates)
		{
			f.Initialise(gameObject);	
		}
		
		focusStates[(int)focusState].OnEnter();	
	}
	
	public void DummyStart()
	{
		PressedState pressedState = new PressedState();
		focusStates.Add(pressedState);
		
		foreach(SFocusState f in focusStates)
		{
			f.DummyInitialise();	
		}
		
		focusStates[(int)focusState].OnEnter();		
	}
	
	// Update is called once per frame
	void Update () {}
	
	public void Focus()
	{
		focusStates[(int)focusState].Focus();
	}
	
	public void Unfocus()
	{
		focusStates[(int)focusState].Unfocus();
	}
	
	public void OnReleased() 
	{
		focusStates[(int)focusState].OnReleased();
	}
	
	public void OnExit()
	{
		focusStates[(int)focusState].OnExit();
	}
	
	public ExampleMenuItem GetNeighbour(EDirection direction)
	{
		return Neighbours[(int)direction];	
	}
	
	public void BeginSwipe(Vector2 position) 
	{
		if(focusStates.Count <= 0) 
		{
			
			PressedState pressedState = new PressedState();
			focusStates.Add(pressedState);
		
			foreach(SFocusState f in focusStates)
			{
			f.DummyInitialise();	
			}
			return;
			//focusStates[0].EndSwipe(position);
		}
		focusStates[(int)focusState].BeginSwipe(position);
	}
	
	public void Dragging(Vector2 position)
	{
		
		if(focusStates.Count <= 0) 
		{
			PressedState pressedState = new PressedState();
			focusStates.Add(pressedState);
		
			foreach(SFocusState f in focusStates)
			{
			f.DummyInitialise();	
			}
			return;
			//focusStates[0].EndSwipe(position);
		}
		focusStates[(int)focusState].Dragging(position);
	}
	
	public void EndSwipe(Vector2 position)
	{
		if(focusStates.Count <= 0) 
		{
			Debug.Log ("Não tenho States-E");
		}
		focusStates[(int)focusState].EndSwipe(position);
	}
	
	public bool IsWithinBounds(string name) { return (this.gameObject.name.CompareTo(name) == 0 ? true : false); }
	
	public bool IsWithinBounds(Vector3 pos)
	{	
		Bounds b;

		if(thisRenderer != null)
		b = thisRenderer.bounds;
		else b = thisRendererS.bounds;
		pos.z = b.center.z; // ignore z		
		return (b.Contains(pos));
	}
	
	public void Tap()
	{ // Tappety-tap!
		Debug.Log("Tap"+gameObject.name);
		SendMessageUpwards("Tap"+gameObject.name, SendMessageOptions.DontRequireReceiver);
	}
	
	public void BeginSwipe() 
	{	
		SendMessageUpwards("Swipe", SendMessageOptions.DontRequireReceiver);		
	}

	public void Move(Vector2 position)
	{
		Vector3 newPosition = Camera.main.ScreenToWorldPoint(new Vector3 (position.x, position.y, this.transform.localPosition.z));
		//this.transform.localPosition = new Vector2 (newPosition.x, this.transform.localPosition.y);
		this.gameObject.GetComponent<AnchorMultiResolution> ().xTransform = newPosition.x;
	}
	
	
	public void onUpdate() {}
}