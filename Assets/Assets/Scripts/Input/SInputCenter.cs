using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum EInputMethod
{
	E_IM_TOUCH,
	E_IM_KEYBOARD,
	E_IM_MOUSE,
	E_IM_KEYBOARD_MOUSE,
	E_IM_ARDUINO_POT,
	E_IM_ANDROID_GAMEPAD,
	E_IM_NONE // DISABLES INPUT
}

public enum EInputCatchMode
{
	E_IC_MENU,
	E_IC_GAME
}

/*
 * 
 * class for initializing, updating and switching input methods / modes
 * 
 * definitions:
 * 	- input METHOD: keyboard / mouse / joystick / ...
 *  - input (catch) MODE: menu / game / ...
 *  
 * */

public class SInputCenter : MonoBehaviour {
	
	public static EInputMethod InputMethod {
		get
		{
			return inputMethod;
		}
		set
		{
			inputMethod = value;
			if(InputChanged != null)
				InputChanged();
		}
	}
	
	public static EInputCatchMode InputCatchMode {
		get
		{
			return inputCatchMode;	
		}
		set
		{
			inputCatchMode = value;
			if(InputChanged != null)
				InputChanged();
		}
	}
	

	private static EInputMethod inputMethod = EInputMethod.E_IM_KEYBOARD_MOUSE;
	
	
	private List<SInputState> inputStates = new List<SInputState>();
	
	private List<SInputState> currentInputs = new List<SInputState>();
	
	private static EInputCatchMode inputCatchMode = EInputCatchMode.E_IC_GAME;
	
	public delegate void InputChangedChandler();
	public static event InputChangedChandler InputChanged;
	
	// debugs are for switching input through editor - remove on release
//	public EInputCatchMode debugInputCatchMode;
//	public EInputMethod debugInputMethod;
	

	
	void Start () {
	
		// set up  states
		STouchState touchState = new STouchState();
		inputStates.Add(touchState);
		SKeyboardState keyboardState = new SKeyboardState();
		inputStates.Add(keyboardState);
		SMouseState mouseState = new SMouseState();
		inputStates.Add(mouseState);
		SArduinoPotState potState = new SArduinoPotState();
		inputStates.Add(potState);
		SGamePadState gamepadState = new SGamePadState();
		inputStates.Add(gamepadState);
		
#if UNITY_IPHONE || UNITY_ANDROID
		if(!Application.isEditor)
			InputMethod = EInputMethod.E_IM_TOUCH;
#endif		
		
		this.SwitchedInput();
		
	}


	
	void Update () {
		// update states
		foreach(SInputState i in currentInputs)
		{
			i.Update();
		}
		
//		if(debugInputMethod != InputMethod) // for switching input through editor
//		{
//			InputMethod = debugInputMethod;
//			this.SwitchedInput();
//		}
		
	}
	

	void SwitchedInput()
	{
		foreach(SInputState i in currentInputs)
		{
			i.OnExit();	
		}
		
		if(InputChanged != null)
			InputChanged();
		
		currentInputs.Clear();
		switch(InputMethod)
		{
			case EInputMethod.E_IM_TOUCH:
				currentInputs.Add(inputStates[(int)EInputState.E_IS_TOUCH]);
				break;
			
			case EInputMethod.E_IM_KEYBOARD:
				currentInputs.Add(inputStates[(int)EInputState.E_IS_KEYBOARD]);
				break;
			
			case EInputMethod.E_IM_MOUSE:
				currentInputs.Add(inputStates[(int)EInputState.E_IS_MOUSE]);
				break;
			
			case EInputMethod.E_IM_KEYBOARD_MOUSE:
				currentInputs.Add(inputStates[(int)EInputState.E_IS_KEYBOARD]);
				currentInputs.Add(inputStates[(int)EInputState.E_IS_MOUSE]);

				//currentInputs.Add(inputStates[(int)EInputState.E_IS_TOUCH]);		
				break;
			case EInputMethod.E_IM_ARDUINO_POT:
				currentInputs.Add(inputStates[(int)EInputState.E_IS_JOYSTICK]);
				break;
			case EInputMethod.E_IM_ANDROID_GAMEPAD:
				currentInputs.Add(inputStates[(int)EInputState.E_IS_GAMEPAD]);
				currentInputs.Add(inputStates[(int)EInputState.E_IS_TOUCH]);
				break;
		}
		
		// init states
		 foreach(SInputState i in currentInputs)
		{
			i.OnStart();
		}
	}
	
	void OnDisable()
	{
		foreach(SInputState i in currentInputs)
			i.OnExit();
	}
		
}
