using UnityEngine;
using System.Collections;

[System.Serializable]
public class SFinger  {
		
	public int id;
	public Vector2 position;
	public Vector2 startPosition;
	
	private Vector2 prevPosition;
	private float prevTime = 0;
	private float curTime = 0;
	
	public SFinger(int id, Vector2 position)
	{
		this.id = id;
		this.startPosition = position;
		this.position = position;
		this.prevPosition = position;
		this.curTime = Time.time;
		this.prevTime = Time.time;
	}
	
	public void Update(Vector2 position)
	{
		this.prevPosition = this.position;
		this.position = position;
		this.prevTime = this.curTime;
		this.curTime = Time.time;
	}
	
	public Vector2 getDeltaPosition()
	{
		return this.position - prevPosition;	
	}
	
	public float getDeltaTime()
	{
		return this.curTime - this.prevTime;	
	}
	
}