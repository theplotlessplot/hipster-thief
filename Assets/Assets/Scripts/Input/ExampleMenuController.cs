/*
 * 
 * 	EXAMPLE MENU CONTROLLER
 * 
 *  change this script to fit your needs - you'll probably need to some serious work in this one
 * 
 *  menuItems register themselves with this script, which then calls their focus / unfocus / tap functions as the input comes in.
 *  for the main menu it'll probably be more interesting to implement groups here - one group per screen (enum screens?) for example - then register your menu items per group 
 *  give each screen an OnEnter and OnExit, in which you respectively initialize and destroy your menu items. Squeeky clean, ne?
 * 
 * */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ExampleMenuController : MonoBehaviour {
	
	public static List<ExampleMenuItem> MenuItems;
	public static ExampleMenuItem Focused;
	public static ExampleMenuItem LastItem;
	//public static ExampleMenuController use;
	private static ExampleMenuItem dummy = new ExampleMenuItem();
	private Camera GUICam;
	
	public static ExampleMenuItem First;
	
	public float minOffsetX = 1;
	public float minOffsetY = 1;
	private bool firstTime = true;
	private bool notSwipe = true;
	private bool notDragging = true;

	float velocity = 0;
	float smoothVelocity = 0;
	float time = 0;
	float deltaTime = 0;
	float minVelocity = 0.02f; // if anything lower than this, velocity gets set to zero
	bool declineVelocity = false;
	Vector2 lastPos = Vector2.zero;
	private int fingerId = -1;
	private Vector3 startPosition;
	private Vector2 previousPos;
	// Use this for initialization
	void Awake () 
	{
		//DontDestroyOnLoad(this);
		SInputCenter.InputChanged += OnInputChanged;
		
		if(MenuItems != null) MenuItems.Clear();
	}
	
	void Start() { SInputCenter.InputCatchMode = EInputCatchMode.E_IC_MENU; }
	
	void OnDisable() 
	{	
		SInputCenter.InputChanged -= OnInputChanged;	
		this.Unsubscribe();
	}
	
	void OnInputChanged() 
	{
		if(SInputCenter.InputCatchMode == EInputCatchMode.E_IC_MENU) this.Subscribe();

		else this.Unsubscribe();	
	}
	
	// Update is called once per frame
	void Update () 
	{ 

	}
	
	public static void RegisterItem(ExampleMenuItem item)
	{
		if(MenuItems == null)
			MenuItems = new List<ExampleMenuItem>();
		
		//Debug.Log ("Registering: " + item.name);
		MenuItems.Add(item);
	}
	
	void SwitchFocus(ExampleMenuItem newFocused)
	{
		if(newFocused == Focused)
			return;

		if(Focused != null)
			Focused.Unfocus();
		
		Focused = newFocused;
		
	//	Debug.Log ("Focused: " + Focused.name);
	}
	
	private ExampleMenuItem CollidedItem(Vector2 screenPos)
	{
		Ray ray;
		RaycastHit hit;
		bool didHit;
		
		GUICam = GameObject.Find("Main Camera").GetComponent<Camera>();
		
		Vector3 worldPoint = GUICam.ScreenToWorldPoint(screenPos);
		ray = GUICam.ScreenPointToRay(screenPos);
		didHit = Physics.Raycast(ray, out hit);
		
		if(MenuItems == null) { Debug.Log ("ADASDADA"); return null; }
		
		foreach(ExampleMenuItem item in MenuItems)
		{
			if(item.listeningForInput && item != null)
			{
				if(hit.collider != null && item.IsWithinBounds(hit.collider.name))
				{
					Debug.Log ("CLIQUEI: " + item.name);
					return item;
				}
			}
		}
		
		return null;
	}
	
	private bool IsWithinFocusBounds(Vector2 screenPos)
	{
		Ray ray;
		RaycastHit hit;
		bool didHit;
		
		//Vector3 worldPoint = GUICam.ScreenToWorldPoint(screenPos);
		ray = GUICam.ScreenPointToRay(screenPos);
		didHit = Physics.Raycast(ray, out hit);
		if(didHit) return (Focused.IsWithinBounds(hit.collider.gameObject.name));
		
		else
		{
			Vector3 worldPoint = GUICam.ScreenToWorldPoint(screenPos);
			return (Focused.IsWithinBounds(worldPoint));
		}
	}
	
	void OnInputStart(int fingerId, Vector2 position) // find any collider and set it focused
	{
		ExampleMenuItem collider = CollidedItem(position);

		if (collider != null && !collider.name.Contains("Player")) 
		{
			Debug.Log ("NOT FOCUS PLAYER");
			SwitchFocus (collider);
			Focused.Focus ();
			LastItem = Focused;
			//Focused.Tap ();
		}

		if (collider != null && collider.name.Contains("Player")) 
		{
			Focused = collider;
			LastItem = Focused;
			Focused.Move(position);
			Debug.Log ("FOCUS PLAYER");
		}
	}
	
	void OnInputUpdate(int fingerId, Vector2 position) // check if still within collider - focus / unfocus if necessary
	{
		if(Focused != null)
		{
			if(Focused != null && Focused.name.Contains("Player"))
				Focused.Move (position);

			if(!IsWithinFocusBounds(position))
			{
				Focused.Unfocus();
				Focused = null;
				Debug.Log ("Still On Void");
			}
			

		}
		
		else
		{
			ExampleMenuItem collider = CollidedItem(position);
			if(collider == null)
			{
				SwitchFocus(collider);
			}
//			if(collider != null && notSwipe)
//			{	
//				SwitchFocus(collider);
//				Focused.Focus ();
//				Focused.BeginSwipe(position);
//				
//				if(Focused != null && Focused.name.Contains("Arrow"))
//				Focused.Tap ();
//				Debug.Log ("Switching");
//				notSwipe = false;
//			}
//		
//			else
//			{
//				if(LastItem != null && !notSwipe)
//				{
//					LastItem.Unfocus();
//					Focused = null;
//					LastItem = null;
//				}
//
//			}
		}
	}
	
	void OnInputReleased(int fingerId, Vector2 position)
	{
		if(LastItem == Focused && (Focused !=null || LastItem != null) && !LastItem.name.Contains("Player"))
		{
			/**Detecção de um tap**/
				Focused.Tap();
				Focused.OnReleased();
				Focused.OnExit();
				Focused.Unfocus();
				LastItem = null;

				firstTime = true;
		}

		if (LastItem == Focused && (Focused != null || LastItem != null) && LastItem.name.Contains ("Player")) 
		{
			LastItem = null;
			Focused = null;
		}
	}

	void OnAxisStart(List<Axis> axes)
	{
		
		if(Focused == null)
		{
			SwitchFocus(FindTopLeftItem());
		}
		else
		{
			foreach(Axis a in axes)
			{
				if(a.name == "Horizontal")
				{
					if(a.val > 0)
						SwitchFocus(Focused.GetNeighbour(EDirection.E_D_RIGHT));	
					
					if(a.val < 0)
						SwitchFocus(Focused.GetNeighbour(EDirection.E_D_LEFT));
				}
				
				if(a.name == "Vertical")
				{
					if(a.val > 0)
						SwitchFocus(Focused.GetNeighbour(EDirection.E_D_UP));
					
					if(a.val < 0)
						SwitchFocus(Focused.GetNeighbour(EDirection.E_D_DOWN));
				}
				
				if(a.name == "Jump")
				{
					if(a.val > 0)
						Focused.Tap();
				}
				
			}
		}
	}
	
	void OnAxisUpdate(List<Axis> axes)
	{
		
	}
	
	void OnAxisRelease(List<Axis> axes)
	{

	}
	
	private void FindNeighbours(ExampleMenuItem item)
	{
		/* 
		 * I find each object's neighbours by calculating positions, which is all I need in the game menu.
		 * for the main menu you might have to rewrite this to start working with tab indexes or something like that. 
		 * 
		 * */
		
		ExampleMenuItem[] neighbours = new ExampleMenuItem[4];
		for (int i = 0; i < neighbours.Length; i++) 
		{
			neighbours[i] = item;
		}
		
		foreach(ExampleMenuItem i in MenuItems)
		{		
			// left
			if(i.myTransform.position.x + minOffsetX < item.myTransform.position.x)
			{	
				if(((item.transform.position.x - i.myTransform.position.x) <= (item.myTransform.position.x - neighbours[(int)EDirection.E_D_LEFT].myTransform.position.x)) || (neighbours[(int)EDirection.E_D_LEFT] == item))
				{
					if((Mathf.Abs((item.transform.position.y - i.myTransform.position.y)) <= Mathf.Abs((item.transform.position.y - neighbours[(int)EDirection.E_D_LEFT].transform.position.y))) || (neighbours[(int)EDirection.E_D_LEFT] == item))
					{
						neighbours[(int)EDirection.E_D_LEFT] = i;
					}
				}

			}
			
			// right
			if(i.myTransform.position.x - minOffsetX > item.myTransform.position.x)
			{
				if(((i.myTransform.position.x - item.myTransform.position.x) <= (neighbours[(int)EDirection.E_D_RIGHT].myTransform.position.x - item.myTransform.position.x)) || (neighbours[(int)EDirection.E_D_RIGHT] == item))
				{
					if((Mathf.Abs((item.transform.position.y - i.myTransform.position.y)) <= Mathf.Abs((item.transform.position.y - neighbours[(int)EDirection.E_D_RIGHT].transform.position.y))) || (neighbours[(int)EDirection.E_D_RIGHT] == item))
					{
						neighbours[(int)EDirection.E_D_RIGHT] = i;
					}
				}
			}
			
			// up
			if(i.myTransform.position.y - minOffsetY > item.myTransform.position.y)
			{
				if(((i.myTransform.position.y - item.myTransform.position.y) <= (neighbours[(int)EDirection.E_D_UP].myTransform.position.y - item.myTransform.position.y)) || (neighbours[(int)EDirection.E_D_UP] == item))
				{
					if((Mathf.Abs((item.transform.position.x - i.myTransform.position.x)) <= Mathf.Abs((item.transform.position.x - neighbours[(int)EDirection.E_D_UP].transform.position.x))) || (neighbours[(int)EDirection.E_D_UP] == item))
					{
						neighbours[(int)EDirection.E_D_UP] = i;
					}
					
				}
			}
			// down
			if(i.myTransform.position.y + minOffsetX < item.myTransform.position.y)
			{
				if(((item.myTransform.position.y - i.myTransform.position.y) <= (item.myTransform.position.y - neighbours[(int)EDirection.E_D_DOWN].myTransform.position.y)) || (neighbours[(int)EDirection.E_D_DOWN] == item))
				{
					if((Mathf.Abs((item.transform.position.x - i.myTransform.position.x)) <= Mathf.Abs((item.transform.position.x - neighbours[(int)EDirection.E_D_DOWN].transform.position.x))) || (neighbours[(int)EDirection.E_D_DOWN] == item))
					{
						neighbours[(int)EDirection.E_D_DOWN] = i;
					}
				}
			}
			
		}
		item.Neighbours = neighbours;
	}
	
	private ExampleMenuItem FindTopLeftItem()
	{
		ExampleMenuItem top = MenuItems[0];
		foreach(ExampleMenuItem i in MenuItems)
		{
			if(i.myTransform.position.y >= top.myTransform.position.y)
			{
				if(i.myTransform.position.x < top.myTransform.position.x)
				{
					top = i;
				}
			}
		}
		return top;
	}
	
	void Subscribe()
	{
		this.Unsubscribe(); // workaround for making sure it's a single subscription from here 
	
		SInputManager.AxisStart += OnAxisStart;
		SInputManager.AxisRelease += OnAxisRelease;
		SInputManager.AxisUpdate += OnAxisUpdate;
		
		SInputManager.InputStart += OnInputStart;
		SInputManager.InputRelease += OnInputReleased;
		SInputManager.InputUpdate += OnInputUpdate;	
		
		if(SInputCenter.InputMethod == EInputMethod.E_IM_KEYBOARD || SInputCenter.InputMethod == EInputMethod.E_IM_KEYBOARD_MOUSE )
		{
			
			//Debug.Log ("SInput:" + MenuItems.Count );
			if(MenuItems == null) return ;
			foreach(ExampleMenuItem item in MenuItems)
			{
				//item.Unfocus();
				FindNeighbours(item);
				
			}
			
			//Focused = FindTopLeftItem();
			//Focused.Focus();
		}
	}
	
	void Unsubscribe()
	{
		SInputManager.AxisStart -= OnAxisStart;
		SInputManager.AxisRelease -= OnAxisRelease;
		SInputManager.AxisUpdate -= OnAxisUpdate;
		
		SInputManager.InputStart -= OnInputStart;
		SInputManager.InputRelease -= OnInputReleased;
		SInputManager.InputUpdate -= OnInputUpdate;	
	}
	
	public static void Show(bool visible)
	{
		foreach(ExampleMenuItem i in MenuItems)
		{
			i.transform.GetComponent<Renderer>().enabled = visible;	
		}
	}
}
