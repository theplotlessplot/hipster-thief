using UnityEngine;
using System.Collections;

/*
 * 
 * class for storing all our focus states, you'll definitely have to extend this one for your needs - send me updates of this file when you change it!
 * 
 * 
 * */

public enum EFocusState
{
	E_FS_PRESSED,
	E_FS_POP_SCALE
}

public class SFocusState : MonoBehaviour {
	
	protected GameObject thisGameObject; 
	protected Color32 initialColor;
	protected Vector3 initialScale;
	protected bool dummy;
	protected bool firstTime = true;

	
	public void Initialise(GameObject thisGameObject)
	{
		this.thisGameObject = thisGameObject;

		if(thisGameObject.GetComponent<Renderer>() != null)
			this.initialColor = thisGameObject.GetComponent<Renderer>().material.color;

		this.initialScale = thisGameObject.transform.localScale;
	}
	
	public void DummyInitialise()
	{
		dummy = true;
	}
	
	public virtual void OnEnter()
	{
		
	}
	
	public virtual void OnExit()
	{
		
	}
	
	public virtual void Focus()
	{
		
	}
	
	public virtual void Unfocus()
	{
		
	}
	
	public virtual void OnReleased()
	{
	
	}
	
	public virtual void BeginSwipe(Vector2 position) 
	{
		
	}
	
	public virtual void EndSwipe(Vector2 position) 
	{
		
		
	}
	
	public virtual void Dragging(Vector2 position) { }
}

public class PressedState : SFocusState // example of a focus state, this one just colors the material red
{
	public override void OnEnter () { }
	
	public override void OnExit () { }
	
	public override void Focus ()
	{
		if (this.thisGameObject.GetComponent<ButtonScript> () != null) 
		{
			this.thisGameObject.GetComponent<ButtonScript> ().Focus ();
		}
		//		new Vector3(initialScale.x*0.9f, initialScale.y, initialScale.z*0.9f));	
		
		//if(AudioManager.instance != null)	AudioManager.instance.PlayFile("fx_tap",false,false,true);
	}
	
	public override void Unfocus ()
	{
		if(this.thisGameObject.GetComponent<ButtonScript>() != null)
			this.thisGameObject.GetComponent<ButtonScript>().UnFocus();
		//		new Vector3(initialScale.x, initialScale.y, initialScale.z));		
	}
	
	public override void OnReleased()
	{	
		//if(this.thisGameObject.GetComponent<ButtonScript>() != null)
		//	this.thisGameObject.GetComponent<ButtonScript>().UnFocus(new Color(initialColor.r,initialColor.g,initialColor.b,initialColor.a),
		//		new Vector3(initialScale.x, initialScale.y, initialScale.z));
		
		
	}
	
	public override void BeginSwipe(Vector2 position) {	}
	
	public override void EndSwipe(Vector2 position)
	{	
	//	Helpers.use.SetChallengesMenu();
	//	Helpers.use.SetTricksMenu();
			
	//	if(Helpers.use.GetTricksMenu() != null) 
	//	{
	//		Helpers.use.GetTricksMenu().MoveList(Helpers.use.detectSwipeDirection());	
	//	}
	}
	
	public override void Dragging(Vector2 position)
	{
	
	}
}