using UnityEngine;
using System.Collections;
using System.Collections.Generic;
/*
 * 
 *  in-between class for catching all input (input states Register / Unregister themselves here)
 *  scripts (joystick, menu, whatever) that need to catch input should subscribe to *ALL* of the events in this class! 
 * 
 * */

public class SInputManager : MonoBehaviour {
	
	public delegate void InputPosition(int finger, Vector2 position);
	public delegate void InputAxis(List<Axis> axes);
	public delegate void InputVector(Vector2 vector);
	
	public static event InputPosition InputStart;
	public static event InputPosition InputUpdate;
	public static event InputPosition InputRelease;
		
	public static event InputAxis AxisStart;
	public static event InputAxis AxisUpdate;
	public static event InputAxis AxisRelease; 
	
	public static event InputVector VectorUpdate;
	
	
	static void OnStart(int fingerId, Vector2 pos)
	{
		if(InputStart != null)
			InputStart(fingerId, pos);
	}
	
	static void OnUpdate(int fingerId, Vector2 pos)
	{
		if(InputUpdate != null)
			InputUpdate(fingerId, pos);
	}
	
	static void OnRelease(int fingerId, Vector2 pos)
	{
		if(InputRelease != null)
			InputRelease(fingerId, pos);
	}
	
	static void OnAxisStart(List<Axis> axes)
	{
		if(AxisStart != null)
		{
			AxisStart(axes);
		}
	}
	
	static void OnVectorUpdate(Vector2 v)
	{
		if(VectorUpdate != null)
			VectorUpdate(v);
	}
	
	static void OnAxisUpdate(List<Axis> axes)
	{
		if(AxisUpdate != null)
			AxisUpdate(axes);
	}
	
	static void OnAxisRelease(List<Axis> axes)
	{
		if(AxisRelease != null)
			AxisRelease(axes);
	}
	
	public static void Register(SInputState state)
	{			
		state.InputStart += OnStart;
		state.InputUpdate += OnUpdate;
		state.InputRelease += OnRelease;
		
		state.AxisStart += OnAxisStart;
		state.AxisUpdate += OnAxisUpdate;
		state.AxisRelease += OnAxisRelease;
		
		state.VectorUpdate += OnVectorUpdate;
	}
	
	public static void Unregister(SInputState state)
	{
		state.InputStart -= OnStart;
		state.InputUpdate -= OnUpdate;
		state.InputRelease -= OnRelease;
		
		state.AxisStart -= OnAxisStart;
		state.AxisUpdate -= OnAxisUpdate;
		state.AxisRelease -= OnAxisRelease;
		
		state.VectorUpdate -= OnVectorUpdate;
	}
	
	// use this to ignore ALL input
	public static void ClearEvents() 
	{
		AxisStart = null;
		AxisUpdate = null;
		AxisRelease = null;
		InputStart = null;
		InputRelease = null;
		InputUpdate = null;
		VectorUpdate = null;
	}
	
}
