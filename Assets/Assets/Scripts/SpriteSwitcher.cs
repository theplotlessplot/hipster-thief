﻿using UnityEngine;
using System.Collections;

public class SpriteSwitcher : MonoBehaviour {

	public GameObject[] _sprite = new GameObject[2];
	public bool _autoSwitcher = false;

	// Use this for initialization
	void Start () {
	
		if (_autoSwitcher)
			StartCoroutine(AutoSwitcher ());
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void ToggleState(bool s)
	{
		_sprite [0].SetActive (s);
		_sprite [1].SetActive (!s);

	}

	public void OtherToggleState(bool s1, bool s2)
	{
		_sprite [0].SetActive (s1);
		_sprite [1].SetActive (s2);

	}

	IEnumerator AutoSwitcher()
	{   
		while (true)
		{   
			AutoToggle();
			yield return new WaitForSeconds(0.5f);

		}   
	}     

	private void AutoToggle()
	{
		_sprite [0].SetActive (!_sprite [0].activeSelf);
		_sprite [1].SetActive (!_sprite [1].activeSelf);

	}
	
}
